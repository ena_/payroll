<?php
//require_once( 'settings.php' );
define( 'PR_OPEN', 0 );
define( 'PR_CLOSED', 1 );
define( 'PR_CANCELLED', 2 );
define( 'PR_REJECTED', 3 );
define( 'PO_OPEN', 0 );
define( 'PO_RECEIVED', 1 );
define( 'PO_INVOICED', 2 );
define( 'PO_CANCELLED', 3 );
define( 'SO_OPEN', 0 );
define( 'SO_DELIVERED', 1 );
define( 'SO_INVOICED', 2 );
define( 'SO_CANCELLED', 3 );
defined( 'DS' ) or define( 'DS', DIRECTORY_SEPARATOR );
define( 'PENJUALAN', 1 );
define( 'CASHOUT', 2 );
define( 'CASHIN', 3 );
define( 'RETURJUAL', 4 );
define( 'SUPPIN', 5 );
define( 'SUPPOUT', 6 );
define( 'TENDER', 7 );
define( 'AUDIT', 8 );
define( 'SJ', 9 );
define( 'CNOTE', 10 );
define( 'SJTAX', 11 );
define( 'RETURBELI', 12 );
define( 'JURNAL_UMUM', 13 );
define( 'BANKTRANSFER', 14 );
define( 'BANKTRANSFERCHARGE', 15 );
define( 'PR', 16 );
define( 'PO_IN', 17 );
define( 'SUPP_INV', 18 );
define( 'SALES_ORDER', 19 );
define( 'SALES_INV', 20 );
define( 'SCNOTE', 21 );
define( 'SUPP_RETURN', 22 );
define( 'CUST_RETURN', 23 );
define( 'SUPP_PAYMENT', 24 );
define( 'CUST_PAYMENT', 25 );
define( 'PERLENGKAPAN_P', 56 );
define( 'PEMBELIAN_BASSET', 57 );
define( 'DOC_QA_FOLDER', './doc/' );
define( 'PRODUKSI', 26 );
define( 'RETURN_PRODUKSI', 27 );
define( 'SELISIHSTOK', 28 );
define( 'CLAIM', 29 );
define( 'MYSQL', 'C:\xampp\mysql\bin\mysql' );
define( 'MYSQLDUMP', 'C:\xampp\mysql\bin\mysqldump' );
define( 'GZIP', 'C:\xampp\gzip' );
define( 'SOAP_CUSTOMER', 'http://localhost:81/siena/query/quote' );
global $systypes_array;
$systypes_array = array(
	PENJUALAN => "Sales",
	CASHOUT   => "Cash Out",
	CASHIN    => "Cash In",
	RETURJUAL => "Return Sales",
	SUPPIN    => "Receive Supplier Item",
	SUPPOUT   => "Return Supplier Item",
	TENDER    => "Tender Declaration",
	AUDIT     => "Audit",
	CNOTE     => "Credit Note",
	RETURBELI => "Retur Beli",
	CLAIM     => "Claim"
);
//	GL account classes
//
define( 'CL_CURRENT_ASSETS', 1 );
define( 'CL_CURRENT_LIABILITIES', 2 );
define( 'CL_EQUITY', 3 );
define( 'CL_INCOME', 4 );
define( 'CL_COGS', 5 );
define( 'CL_EXPENSE', 6 );
define( 'CL_OTHER_INCOME', 7 );
define( 'CL_FIXED_ASSETS', 8 );
define( 'CL_LONGTERM_LIABILITIES', 9 );
$class_types = array(
	CL_CURRENT_ASSETS       => "Current Assets",
	CL_FIXED_ASSETS         => "Fixed Assets",
	CL_CURRENT_LIABILITIES  => "Current Liabilities",
	CL_LONGTERM_LIABILITIES => "Longterm Liabilities",
	CL_EQUITY               => "Equity",
	CL_INCOME               => "Income",
	CL_COGS                 => "Cost of Goods Sold",
	CL_EXPENSE              => "Cost",
	CL_OTHER_INCOME         => "Other Income and Expenses",
);
/**
 * This is the shortcut to Yii::app()
 */
function app() {
	return Yii::app();
}
/**
 * @param $number
 *
 * @return int
 */
function is_angka( $number ) {
	if ( strlen( $number ) == 0 ) {
		return false;
	}
	switch ( gettype( $number ) ) {
		case "NULL":
			return false;
		case "resource":
			return false;
		case "object":
			return false;
		case "array":
			return false;
		case "unknown type":
			return false;
	}
	return preg_match( "/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number );
}
/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs() {
	// You could also call the client script instance via Yii::app()->clientScript
	// But this is faster
	return Yii::app()->getClientScript();
}
/**
 * This is the shortcut to Yii::app()->user.
 */
function user() {
	return Yii::app()->getUser();
}
/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url( $route, $params = array(), $ampersand = '&' ) {
	return Yii::app()->createUrl( $route, $params, $ampersand );
}
/**
 * This is the shortcut to CHtml::encode
 */
function h( $text ) {
	return htmlspecialchars( $text, ENT_QUOTES, Yii::app()->charset );
}
function dbTrans() {
	return Yii::app()->db->beginTransaction();
}
/**
 * This is the shortcut to CHtml::link()
 */
function l( $text, $url = '#', $htmlOptions = array() ) {
	return CHtml::link( $text, $url, $htmlOptions );
}
/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t( $message, $category = 'stay', $params = array(), $source = null, $language = null ) {
	return Yii::t( $category, $message, $params, $source, $language );
}
/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu( $url = null ) {
	static $baseUrl;
	if ( $baseUrl === null ) {
		$baseUrl = Yii::app()->getRequest()->getBaseUrl();
	}
	return $url === null ? $baseUrl : $baseUrl . '/' . ltrim( $url, '/' );
}
/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param( $name ) {
	return Yii::app()->params[ $name ];
}
function Encrypt( $string ) {
	return base64_encode( Yii::app()->getSecurityManager()->encrypt( $string ) );
}
function Decrypt( $string ) {
	return Yii::app()->getSecurityManager()->decrypt( base64_decode( $string ) );
}
function Now( $formatDate = 'yyyy-MM-dd' ) {
	return get_date_today( $formatDate ) . ' ' . get_time_now();
}
function generatePassword( $length = 8 ) {
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	$count = mb_strlen( $chars );
	for ( $i = 0, $result = ''; $i < $length; $i ++ ) {
		$index  = rand( 0, $count - 1 );
		$result .= mb_substr( $chars, $index, 1 );
	}
	return $result;
}
function date2longperiode( $date, $format ) {
	$timestamp = CDateTimeParser::parse( $date, 'yyyy-MM-dd' );
	$formater  = new CDateFormatter( 'id_ID' );
	return $formater->format( $format, $timestamp );
}
function period2date( $month, $year ) {
	$timestamp = DateTime::createFromFormat( 'd/m/Y', "01/$month/$year" );
	$start     = $timestamp->format( 'Y-m-d' );
	$end       = $timestamp->format( 'Y-m-t' );
	return array( 'start' => $start, 'end' => $end );
}
function get_jemaat_from_user( $nij ) {
	return Jemaat::model()->findByPk( $nij );
}
function get_number( $number ) {
	return str_replace( ",", "", $number );
}
function get_jemaat_from_user_id( $id ) {
	$user = Users::model()->findByPk( $id );
	if ( $user == null ) {
		return false;
	} else {
		return get_jemaat_from_user( $user->nij );
	}
}
function sql2date( $date, $format = 'dd/MM/yyyy' ) {
	$timestamp = CDateTimeParser::parse( $date, 'yyyy-MM-dd' );
	return Yii::app()->dateFormatter->format( $format, $timestamp );
}
function date2sql( $date ) {
	$timestamp = CDateTimeParser::parse( $date, 'dd/MM/yyyy' );
	return Yii::app()->dateFormatter->format( 'yyyy-MM-dd', $timestamp );
}
function sql2long_date( $date ) {
	$timestamp = CDateTimeParser::parse( $date, 'yyyy-MM-dd' );
	$formater  = new CDateFormatter( 'id_ID' );
	return $formater->formatDateTime( $timestamp, 'long', false );
}
function get_date_tomorrow() {
	return Yii::app()->dateFormatter->format( 'yyyy-MM-dd', time() + ( 1 * 24 * 60 * 60 ) );
}
function get_time_now() {
	return Yii::app()->dateFormatter->format( 'HH:mm:ss', time() );
}
function get_date_today( $format = 'yyyy-MM-dd' ) {
	return Yii::app()->dateFormatter->format( $format, time() );
}
function percent_format( $value, $decimal = 0 ) {
	return number_format( $value * 100, $decimal ) . '%';
}
function curr_format( $value, $decimal = 0 ) {
	return "Rp" . number_format( $value * 100, $decimal );
}
function acc_format( $value, $decimal = 0 ) {
	$normalize = $value < 0 ? - $value : $value;
	$print     = number_format( $normalize, $decimal );
	return $value < 0 ? "($print)" : $print;
}
function round_up( $number, $precision = 2 ) {
	$number = round( $number, $precision );
	$fig    = (int) str_pad( '1', $precision, '0' );
	return ( ceil( $number * $fig ) / $fig );
}
function round_down( $number, $precision = 2 ) {
	$number = round( $number, $precision );
	$fig    = (int) str_pad( '1', $precision, '0' );
	return ( floor( $number * $fig ) / $fig );
}

function round_up_to($int, $n)
{
	return ceil($int / $n) * $n;
}
function mysql2excel( $myql_date ) {
	return strtotime( $myql_date ) + ( ( strtotime( '1970-01-01' ) - strtotime( '1900-01-01' ) ) * 86400 );
}
function terbilang( $satuan ) {
	$huruf = array(
		"",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan",
		"sepuluh",
		"sebelas"
	);
	if ( $satuan < 12 ) {
		return " " . $huruf[ $satuan ];
	} elseif ( $satuan < 20 ) {
		return terbilang( $satuan - 10 ) . " belas";
	} elseif ( $satuan < 100 ) {
		return terbilang( $satuan / 10 ) . " puluh" . terbilang( $satuan % 10 );
	} elseif ( $satuan < 200 ) {
		return "seratus" . terbilang( $satuan - 100 );
	} elseif ( $satuan < 1000 ) {
		return terbilang( $satuan / 100 ) . " ratus" . terbilang( $satuan % 100 );
	} elseif ( $satuan < 2000 ) {
		return "seribu" . terbilang( $satuan - 1000 );
	} elseif ( $satuan < 1000000 ) {
		return terbilang( $satuan / 1000 ) . " ribu" . terbilang( $satuan % 1000 );
	} elseif ( $satuan < 1000000000 ) {
		return terbilang( $satuan / 1000000 ) . " juta" . terbilang( $satuan % 1000000 );
	} elseif ( $satuan < 1000000000000 ) {
		return terbilang( $satuan / 1000000000 ) . " milyar" . terbilang( $satuan % 1000000000 );
	} elseif ( $satuan >= 1000000000000 ) {
		echo "Angka yang Anda masukkan terlalu besar";
	}
}
function format_number_report( $num, $digit = 0 ) {
	if ( ! is_angka( $num ) ) {
		return $num;
	}
	return ( isset( $_POST['format'] ) && $_POST['format'] == 'excel' ) ? $num : number_format( $num, $digit );
}
function is_report_excel() {
	return ( isset( $_POST['format'] ) && $_POST['format'] == 'excel' );
}
/**
 * Check the syntax of some PHP code.
 *
 * @param string $code PHP code to check.
 *
 * @return boolean|array If false, then check was successful, otherwise an array(message,line) of errors is returned.
 */
function php_syntax_error( $code ) {
	if ( ! defined( "CR" ) ) {
		define( "CR", "\r" );
	}
	if ( ! defined( "LF" ) ) {
		define( "LF", "\n" );
	}
	if ( ! defined( "CRLF" ) ) {
		define( "CRLF", "\r\n" );
	}
	$braces   = 0;
	$inString = 0;
	foreach ( token_get_all( '<?php ' . $code ) as $token ) {
		if ( is_array( $token ) ) {
			switch ( $token[0] ) {
				case T_CURLY_OPEN:
				case T_DOLLAR_OPEN_CURLY_BRACES:
				case T_START_HEREDOC:
					++ $inString;
					break;
				case T_END_HEREDOC:
					-- $inString;
					break;
			}
		} else if ( $inString & 1 ) {
			switch ( $token ) {
				case '`':
				case '\'':
				case '"':
					-- $inString;
					break;
			}
		} else {
			switch ( $token ) {
				case '`':
				case '\'':
				case '"':
					++ $inString;
					break;
				case '{':
					++ $braces;
					break;
				case '}':
					if ( $inString ) {
						-- $inString;
					} else {
						-- $braces;
						if ( $braces < 0 ) {
							break 2;
						}
					}
					break;
			}
		}
	}
	$inString      = @ini_set( 'log_errors', false );
	$token         = @ini_set( 'display_errors', true );
	$token_startup = @ini_set( 'display_startup_errors', false );
	ob_start();
	$code = substr( $code, strlen( '<?php ' ) );
	$braces || $code = "if(0){{$code}\n}";
	if ( @eval( $code ) === false ) {
		if ( $braces ) {
			$braces = PHP_INT_MAX;
		} else {
			false !== strpos( $code, CR ) && $code = strtr( str_replace( CRLF, LF, $code ), CR, LF );
			$braces = substr_count( $code, LF );
		}
		$code = ob_get_clean();
		$code = strip_tags( $code );
		if ( preg_match( "'syntax error, (.+) in .+ on line (\d+)$'s", $code, $code ) ) {
			$code[2] = (int) $code[2];
			$code    = $code[2] <= $braces
				? array( $code[1], $code[2] )
				: array( 'unexpected $end' . substr( $code[1], 14 ), $braces );
		} else {
			$code = array( 'syntax error', 0 );
		}
	} else {
		ob_end_clean();
		$code = false;
	}
	@ini_set( 'display_startup_errors', $token_startup );
	@ini_set( 'display_errors', $token );
	@ini_set( 'log_errors', $inString );
	return $code;
}
function checkSyntax( $fileContent, $use_eval = false ) {
	error_reporting( E_ALL ^ E_NOTICE );
	$dirName = Yii::getPathOfAlias( 'application.runtime.' . Yii::app()->controller->id );
	if ( $use_eval === false ) {
		$filename = tempnam( $dirName, '_' );
		file_put_contents( $filename, '<?php ' . $fileContent );
//		$exists = file_exists($filename);
		exec( "php -l {$filename}", $output, $return );
		if ( $return == 0 ) {
			return true;
		}
		return $output;
//		$output = trim( implode( PHP_EOL, $output ) );
//		echo "Return: {$return}" . PHP_EOL;
//		echo "Output '{$output}'" . PHP_EOL . PHP_EOL . PHP_EOL;
	} else {
		try {
			eval( 'return true;function testEval(){' . $fileContent . '}' );
		} catch ( ParseError $e ) {
			return false;
		}
		return true;
	}
}
function format_number_print( $number, $str_pad = ' ', $lpad = 15, $prefix = 'Rp ', $decimals = 0, $dec_point = '.', $thousands_sep = ',' ) {
	return $prefix . str_pad( number_format( $number, $decimals, $dec_point, $thousands_sep ), $lpad, $str_pad, STR_PAD_LEFT );
}
function console( $command, $action, $option = null, $log ) {
	define( 'PHP_EXE', 'php' );
	$log  = Yii::app()->basePath . DS . 'runtime' . DS . $log;
	$yiic = dirname( __file__ ) . DIRECTORY_SEPARATOR . 'yiic';
	$cmd  = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $option . ' >> ' . $log . ' 2>&1';
	if ( substr( php_uname(), 0, 7 ) == "Windows" ) {
		pclose( popen( 'start /B cmd /C "' . $cmd . '"', "r" ) );
	} else {
		exec( $cmd . ' &' );
	}
}