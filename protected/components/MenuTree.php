<?php
class MenuTree {
	var $security_role;
	var $menu_users = array(
		'text' => 'User Manajement',
		'id'   => 'jun.UsersGrid',
		'leaf' => true
	);
	var $security = array(
		'text' => 'Security Roles',
		'id'   => 'jun.SecurityRolesGrid',
		'leaf' => true
	);
	function __construct( $id ) {
		$role                = SecurityRoles::model()->findByPk( $id );
		$this->security_role = explode( ",", $role->sections );
	}
	function getChildMaster() {
		$child = array();
		if ( in_array( 128, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Bisnis Unit',
				'id'   => 'jun.BuGrid',
				'leaf' => true
			);
		}
		if ( in_array( 115, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Area',
				'id'   => 'jun.AreaGrid',
				'leaf' => true
			);
		}
		if ( in_array( 125, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Cabang',
				'id'   => 'jun.CabangGrid',
				'leaf' => true
			);
		}
		if ( in_array( 117, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Leveling',
				'id'   => 'jun.LevelingGrid',
				'leaf' => true
			);
		}
		if ( in_array( 118, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Golongan',
				'id'   => 'jun.GolonganGrid',
				'leaf' => true
			);
		}
		if ( in_array( 124, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Jabatan',
				'id'   => 'jun.JabatanGrid',
				'leaf' => true
			);
		}
		if ( in_array( 116, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Status',
				'id'   => 'jun.StatusGrid',
				'leaf' => true
			);
		}
		if ( in_array( 123, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Status Pegawai',
				'id'   => 'jun.StatusPegawaiGrid',
				'leaf' => true
			);
		}
//        if (in_array(126, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Status',
//                'id' => 'jun.StatusGrid',
//                'leaf' => true
//            );
//        }
		if ( in_array( 101, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Shift',
				'id'   => 'jun.ShiftGrid',
				'leaf' => true
			);
		}
		if ( in_array( 102, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Shift Per Hari',
				'id'   => 'jun.DayShiftGrid',
				'leaf' => true
			);
		}
		if ( in_array( 103, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Keterangan',
				'id'   => 'jun.KeteranganGrid',
				'leaf' => true
			);
		}
//        if (in_array(104, $this->security_role)) {
//            $child[] = array(
//                'text' => 'Shift Per Person',
//                'id' => 'jun.ShiftPinGrid',
//                'leaf' => true
//            );
//        }

		if ( in_array( 134, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Bank',
				'id'   => 'jun.BankGrid',
				'leaf' => true
			);
		}
		if ( in_array( 129, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Jenis Periode',
				'id'   => 'jun.JenisPeriodeGrid',
				'leaf' => true
			);
		}
		if ( in_array( 105, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Periode',
				'id'   => 'jun.PeriodeGrid',
				'leaf' => true
			);
		}
		if ( in_array( 133, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Periode Join',
				'id'   => 'jun.PeriodeJoinGrid',
				'leaf' => true
			);
		}
		if ( in_array( 119, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Komponen Gaji',
				'id'   => 'jun.MasterGrid',
				'leaf' => true
			);
		}
		if ( in_array( 120, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Master Gaji',
				'id'   => 'jun.MasterGajiGrid',
				'leaf' => true
			);
		}
		if ( in_array( 127, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Schema',
				'id'   => 'jun.SchemaGrid',
				'leaf' => true
			);
		}
		if ( in_array( 121, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Formula',
				'id'   => 'jun.SchemaGajiGrid',
				'leaf' => true
			);
		}
		if ( in_array( 122, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Pegawai',
				'id'   => 'jun.PegawaiGrid',
				'leaf' => true
			);
		}
		if ( in_array( 132, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Resign',
				'id'   => 'jun.ResignGrid',
				'leaf' => true
			);
		}
		if ( in_array( 130, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Fix Add',
				'id'   => 'jun.FixaddGrid',
				'leaf' => true
			);
		}
		if ( in_array( 131, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Fix Add Value',
				'id'   => 'jun.AddValueGrid',
				'leaf' => true
			);
		}
		if ( in_array( 135, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Sinkronisasi',
				'id'   => 'jun.HistorySinkronGrid',
				'leaf' => true
			);
		}
		if ( in_array( 136, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Template Email',
				'id'   => 'jun.EmailTransGrid',
				'leaf' => true
			);
		}
		return $child;
	}
	function getMaster( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Master',
			'expanded' => false,
			'children' => $child
		);
	}
	function getTransaction( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Transaction',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildTransaction() {
		$child = array();
		if ( in_array( 224, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Assign Shift',
				'id'   => 'jun.AssignShiftGrid',
				'leaf' => true
			);
		}
		if ( in_array( 223, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Review Absensi Fase 1',
				'id'   => 'jun.FpGrid',
				'leaf' => true
			);
		}
		if ( in_array( 225, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Review Absensi Fase 2',
				'id'   => 'jun.ValidasiGrid',
				'leaf' => true
			);
		}
		if ( in_array( 227, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Absensi Payroll',
				'id'   => 'jun.PayrollAbsensiGrid',
				'leaf' => true
			);
		}
		if ( in_array( 229, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Payroll Worksheet',
				'id'   => 'jun.PayrollWorksheetGrid',
				'leaf' => true
			);
		}
		if ( in_array( 228, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Lock Payroll',
				'id'   => 'jun.LockPayroll',
				'leaf' => true
			);
		}
		if ( in_array( 226, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Payroll',
				'id'   => 'jun.PayrollGrid',
				'leaf' => true
			);
		}
		if ( in_array( 230, $this->security_role ) ) {
			$child[] = array(
				'text' => 'History Email',
				'id'   => 'jun.EmailTransHistoryGrid',
				'leaf' => true
			);
		}
		return $child;
	}
	function getReport( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Report',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildReport() {
		$child = array();
		if ( in_array( 301, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Finger Print',
				'id'   => 'jun.ReportFp',
				'leaf' => true
			);
		}
		if ( in_array( 301, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Absen',
				'id'   => 'jun.ReportAbsen',
				'leaf' => true
			);
		}
		if ( in_array( 302, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Rekap Pegawai',
				'id'   => 'jun.ReportRekapPegawai',
				'leaf' => true
			);
		}
		if ( in_array( 305, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Master Gaji',
				'id'   => 'jun.ReportMasterGaji',
				'leaf' => true
			);
		}
		if ( in_array( 303, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Rekap Payroll',
				'id'   => 'jun.ReportRekapPayroll',
				'leaf' => true
			);
		}
		if ( in_array( 304, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Rekap Presensi',
				'id'   => 'jun.ReportRekapAbsensi',
				'leaf' => true
			);
		}
		if ( in_array( 302, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Mandiri',
				'id'   => 'jun.ReportMandiri',
				'leaf' => true
			);
		}
		if ( in_array( 306, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Report Bank',
				'id'   => 'jun.ReportLaporanBank',
				'leaf' => true
			);
		}
		return $child;
	}
	function getAdministration( $child ) {
		if ( empty( $child ) ) {
			return array();
		}
		return array(
			'text'     => 'Administration',
			'expanded' => false,
			'children' => $child
		);
	}
	function getChildAdministration() {
		$child = array();
		if ( in_array( 401, $this->security_role ) ) {
			$child[] = array(
				'text' => 'User Management',
				'id'   => 'jun.UsersGrid',
				'leaf' => true
			);
		}
		if ( in_array( 402, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Security Roles',
				'id'   => 'jun.SecurityRolesGrid',
				'leaf' => true
			);
		}
		if ( in_array( 403, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Backup / Restore',
				'id'   => 'jun.BackupRestoreWin',
				'leaf' => true
			);
		}
//		if ( in_array( 404, $this->security_role ) ) {
//			$child[] = array(
//				'text' => 'Email',
//				'id'   => 'jun.EmailWin',
//				'leaf' => true
//			);
//		}
		return $child;
	}
	function getGeneral() {
		$username = Yii::app()->user->name;
		$child    = array();
		if ( in_array( 001, $this->security_role ) ) {
			$child[] = array(
				'text' => 'Change Password',
				'id'   => 'jun.PasswordWin',
				'leaf' => true
			);
		}
		if ( in_array( 002, $this->security_role ) ) {
			$child[] = array(
				'text' => "Logout ($username)",
				'id'   => 'logout',
				'leaf' => true
			);
		}
		return $child;
	}
	public function get_menu() {
		$data   = array();
		$master = self::getMaster( self::getChildMaster() );
		if ( ! empty( $master ) ) {
			$data[] = $master;
		}
		$trans = self::getTransaction( self::getChildTransaction() );
		if ( ! empty( $trans ) ) {
			$data[] = $trans;
		}
		$report = self::getReport( self::getChildReport() );
		if ( ! empty( $report ) ) {
			$data[] = $report;
		}
		$adm = self::getAdministration( self::getChildAdministration() );
		if ( ! empty( $adm ) ) {
			$data[] = $adm;
		}
		$username = Yii::app()->user->name;
		if ( in_array( 001, $this->security_role ) ) {
			$data[] = array(
				'text' => 'Change Password',
				'id'   => 'jun.PasswordWin',
				'leaf' => true
			);
		}
		if ( in_array( 002, $this->security_role ) ) {
			$data[] = array(
				'text' => "Logout ($username)",
				'id'   => 'logout',
				'leaf' => true
			);
		}
		return CJSON::encode( $data );
	}
}
