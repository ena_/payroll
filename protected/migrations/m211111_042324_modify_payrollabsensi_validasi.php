<?php

class m211111_042324_modify_payrollabsensi_validasi extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('
			ALTER TABLE "public"."pbu_payroll_absensi" ADD COLUMN "wfh" int2 DEFAULT 0 NOT NULL;
		')->execute();
		Yii::app()->db->createCommand('		
			ALTER TABLE "public"."pbu_payroll_absensi" ADD COLUMN "total_hari" int2 DEFAULT 0 NOT NULL;
		')->execute();
		Yii::app()->db->createCommand('		
			ALTER TABLE "public"."pbu_payroll_absensi" ADD COLUMN "cabang_id" varchar(36) COLLATE "default";
		')->execute();
		Yii::app()->db->createCommand('		
			ALTER TABLE "public"."pbu_validasi" ADD COLUMN "status_wfh" int4;
		')->execute();
	}

	public function down()
	{
		echo "m211111_042324_modify_payrollabsensi_validasi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}