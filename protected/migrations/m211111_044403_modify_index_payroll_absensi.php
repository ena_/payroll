<?php

class m211111_044403_modify_index_payroll_absensi extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('		
			DROP INDEX "public"."idx_21872_Index 4";
		')->execute();
		
		Yii::app()->db->createCommand('		
			CREATE UNIQUE INDEX "idx_21872_Index 4" ON "public"."pbu_payroll_absensi" USING btree ("periode_id", "pegawai_id", "cabang_id");
		')->execute();
	}

	public function down()
	{
		echo "m211111_044403_modify_index_payroll_absensi does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}