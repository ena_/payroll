<?php
Yii::import( 'application.models._base.BaseAddValue' );
class AddValue extends BaseAddValue {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->add_value_id == null ) {
			$command            = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid               = $command->queryScalar();
			$this->add_value_id = $uuid;
		}
		return parent::beforeValidate();
	}
}