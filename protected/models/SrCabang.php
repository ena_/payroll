<?php

Yii::import('application.models._base.BaseSrCabang');

class SrCabang extends BaseSrCabang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->sr_cabang == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sr_cabang = $uuid;
        }
        return parent::beforeValidate();
    }
}