<?php
Yii::import( 'application.models._base.BasePegawai' );
class Pegawai extends BasePegawai {
	const GROUP = 'nik';
	const VALUE = '?';
	const DIGIT = 6;
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public static function getByNIK( $NIK, $bu_id ) {
		$criteria        = new CDbCriteria();
		$criteria->alias = 'pe';
		$criteria->join  = 'INNER JOIN pbu_cabang AS pc ON pe.cabang_id = pc.cabang_id';
		$criteria->addCondition( 'pe.nik = :nik' );
		$criteria->addCondition( 'pc.bu_id = :bu_id' );
		$criteria->params[':nik']   = $NIK;
		$criteria->params[':bu_id'] = $bu_id;
		$pegawai                    = Pegawai::model()->find( $criteria );
		return $pegawai;
	}
	public function isHaveBankType( $tipe_bank ) {
		$bank = PegawaiBank::model()->findByAttributes( [
			'pegawai_id' => $this->pegawai_id,
			'tipe_bank'  => $tipe_bank
		] );
		return $bank != null;
	}
	public function getRekeningBank( $bankName, $tipeBank ) {
		$criteria        = new CDbCriteria();
		$criteria->alias = 'pb';
		$criteria->join  = 'LEFT JOIN pbu_bank as b ON b.bank_id = pb.bank_id';
		$criteria->addCondition( 'pb.pegawai_id = :pegawai_id' );
		$criteria->params[':pegawai_id'] = $this->pegawai_id;
		$criteria->addCondition( 'lower(pb.tipe_bank) = :tipe_bank' );
		$criteria->params[':tipe_bank'] = strtolower( $tipeBank );
		$criteria->addCondition( 'lower(b.nama_bank) = :nama_bank' );
		$criteria->params[':nama_bank'] = strtolower( $bankName );
		/** @var PegawaiBank $bank */
		$bank = PegawaiBank::model()->find( $criteria );
		if ( $bank != null ) {
			return $bank->no_rekening;
		}
		return '';
	}
//    public function behaviors()
//    {
//        return array(
//            array(
//                'class' => 'ext.autonumber.MdmAutonumberBehavior',
//                'attribute' => 'nik', // required
//                'group' => 'nik', // required, unique
//                'value' => '?', // format auto number. '?' will be replaced with generated number
//                'digit' => 6 // optional, default to null.
//            )
//        );
//    }
	public function beforeValidate() {
		if ( $this->pegawai_id == null ) {
			$command          = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid             = $command->queryScalar();
			$this->pegawai_id = $uuid;
		}
		if ( $this->tuser == null ) {
			$this->tuser = Yii::app()->user->id;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->last_update_id == null ) {
			$this->last_update_id = Yii::app()->user->id;
		}
		if ( $this->last_update == null ) {
			$this->last_update = new CDbExpression( 'NOW()' );
		}
//		if ( $this->cabang_id != null ) {
//			$store        = Cabang::model()->findByPk( $this->cabang_id );
//			if($store == null){
//				$this->addError('Store','Cabang tidak ditemukan.');
//				return false;
//			}
//			$this->store = $store->kode_cabang;
//		}
		return parent::beforeValidate();
	}
	public function getKodeLevel() {
		if ( $this->leveling != null ) {
			return $this->leveling->kode;
		}
		return '';
	}
	public function getNamaLevel() {
		if ( $this->leveling != null ) {
			return $this->leveling->nama;
		}
		return '';
	}
	public function getKodeGolongan() {
		if ( $this->golongan != null ) {
			return $this->golongan->kode;
		}
		return '';
	}
	public function getNamaGolongan() {
		if ( $this->golongan != null ) {
			return $this->golongan->nama;
		}
		return '';
	}
	public function getNamaJabatan() {
		if ( $this->jabatan_id != null ) {
			/** @var Jabatan $jabatan */
			$jabatan = Jabatan::model()->findByPk( $this->jabatan_id );
			if ( $jabatan != null ) {
				return $jabatan->nama_jabatan;
			}
		}
		return '';
	}
	public function getCabangUMK() {
		if ( $this->cabang != null ) {
			return $this->cabang->umk;
		}
		return 0;
	}
	/**
	 * @param Periode $periode
	 *
	 * @return bool|string
	 * @throws CException
	 */
	public function generateWorksheet( $periode ) {
		$msg = 'Data berhasil digenerate.';
//		app()->db->autoCommit = false;
		$transaction = Yii::app()->db->beginTransaction();
		try {
			global $PEGAWAI__;
			$PEGAWAI__ = $this;
			$phpCode   = '';
//			$phpCode .= '$PEGAWAI__=' . $this . ';' . PHP_EOL;
			$phpCode      .= '$LEVEL__=\'' . $this->getKodeLevel() . '\';' . PHP_EOL;
			$phpCode      .= '$GOLONGAN__=\'' . $this->getKodeGolongan() . '\';' . PHP_EOL;
			$phpCode      .= '$NAMALEVEL__=\'' . $this->getNamaLevel() . '\';' . PHP_EOL;
			$phpCode      .= '$NAMAGOLONGAN__=\'' . $this->getNamaGolongan() . '\';' . PHP_EOL;
			$phpCode      .= '$UMK__=' . $this->getCabangUMK() . ';' . PHP_EOL;
			$phpCode      .= '$JABATAN__=\'' . $this->getNamaJabatan() . '\';' . PHP_EOL;
			$join         = new DateTime( $this->tgl_masuk );
			$periode_date = new DateTime( $periode->periode_end );
			$diff         = $join->diff( $periode_date )->format( "%R%a" );
			$phpCode      .= '$MASAKERJA__=' . intval( $diff ) . ';' . PHP_EOL;
			$c            = new CDbCriteria();
			$c->addCondition( 'pegawai_id = :pegawai_id' );
			$c->addCondition( 'periode_id = :periode_id' );
			$c->addInCondition( 'trans_type', [
				PayrollWorksheet::SCHEMA_GAJI,
				PayrollWorksheet::SCHEMA_DAILY,
				PayrollWorksheet::SCHEMA_JOIN,
				PayrollWorksheet::ADDED_VALUE,
			] );
			$c->params[':pegawai_id'] = $this->pegawai_id;
			$c->params[':periode_id'] = $periode->periode_id;
			PayrollWorksheet::model()->deleteAll( $c );
			$dirName = Yii::getPathOfAlias( 'application.runtime.' . Yii::app()->controller->id );
			if ( ! is_dir( $dirName ) ) {
				mkdir( $dirName, 0777 );
			}
			/** @var Cabang $cab */
			$cab = Cabang::model()->findByPk( $this->cabang_id );
			if ( $cab == null ) {
				throw new Exception( 'Cabang tidak ditemukan.' );
			}
			/** @var Area $area */
			$area = Area::model()->findByPk( $cab->area_id );
			if ( $area == null ) {
				throw new Exception( 'Area tidak ditemukan.' );
			}
			/** @var Jabatan $jab */
//			$jab = Jabatan::model()->findByPk( $this->jabatan_id );
//			if ( $jab == null ) {
//				throw new Exception( 'Jabatan tidak ditemukan.' );
//			}
			if ( $this->leveling_id == null ) {
				throw new Exception( 'Leveling tidak ditemukan.' );
			}
			/** @var PayrollAbsensi $absensi */
			$absensi = PayrollAbsensi::model()->findByAttributes( [
				'periode_id' => $periode->periode_id,
				'pegawai_id' => $this->pegawai_id
			] );
			if ( $absensi == null ) {
				throw new Exception( 'Absensi tidak ditemukan.' );
			}
			$phpCode .= '$PHJ__=' . $periode->getCount() . ';' . PHP_EOL;
			$phpCode .= '$PJOFF__=' . $periode->jumlah_off . ';' . PHP_EOL;
			$phpCode .= '$HK__=' . $absensi->total_hari_kerja . ';' . PHP_EOL;
			$phpCode .= '$LK__=' . $absensi->total_lk . ';' . PHP_EOL;
			$phpCode .= '$CT__=' . $absensi->total_cuti_tahunan . ';' . PHP_EOL;
			$phpCode .= '$CM__=' . $absensi->total_cuti_menikah . ';' . PHP_EOL;
			$phpCode .= '$CS__=' . $absensi->total_cuti_bersalin . ';' . PHP_EOL;
			$phpCode .= '$CI__=' . $absensi->total_cuti_istimewa . ';' . PHP_EOL;
			$phpCode .= '$CNA__=' . $absensi->total_cuti_non_aktif . ';' . PHP_EOL;
			$phpCode .= '$GOFF__=' . $absensi->total_off . ';' . PHP_EOL;
			$phpCode .= '$JOFF__=' . $absensi->jatah_off . ';' . PHP_EOL;
			$phpCode .= '$SICK__=' . $absensi->total_sakit . ';' . PHP_EOL;
			$phpCode .= '$LA__=' . $absensi->total_lembur_1 . ';' . PHP_EOL;
			$phpCode .= '$LN__=' . $absensi->total_lembur_next . ';' . PHP_EOL;
			$phpCode .= '$LT__=' . $absensi->less_time . ';' . PHP_EOL;
			$phpCode .= '$PP__=' . $absensi->total_presentasi . ';' . PHP_EOL;
			/** @var Master[] $master */
			$master = Master::model()->findAllByAttributes( [
				'bu_id' => $periode->jenisPeriode->bu_id
			] );
			foreach ( $master as $md1 ) {
				$phpCode .= '$' . $md1->kode . '=0;' . PHP_EOL;
			}
			$length   = 10;
			$tmpfname = tempnam( $dirName, 'tpay' ) . 'php';
			file_put_contents( $tmpfname, "<?php" . PHP_EOL . $phpCode );
			include $tmpfname;
			#### DAILY
			$tmpscdfname     = tempnam( $dirName, 'tscpay' ) . '.php';
			$tmpfnamemasterD = tempnam( $dirName, 'tpay' ) . 'php';
//			file_put_contents($tmpfname.'put',"<?php" . PHP_EOL . $phpCode);
			/** @var SchemaGajiView[] $schema */
			$schema = SchemaGajiView::model()->findAllByAttributes( [
				'status_id'        => $this->status_id,
				'jenis_periode_id' => $this->jenis_periode_id,
				'rotation'         => 'D'
			] );
			if ( $schema != null ) {
				foreach ( $schema as $sc ) {
//					$pegawai__ = $this;
					/** @var PegawaiValidasi[] $validasi */
					$validasi = PegawaiValidasi::model()->findAllByAttributes( [
						'pegawai_id' => $this->pegawai_id
					] );
					if ( $validasi != null ) {
						foreach ( $validasi as $absen_daily ) {
							## SETTING VARIABLE
							$dIn_time__             = $absen_daily->in_time;
							$dOut_time__            = $absen_daily->out_time;
							$d_Min_early_time__     = $absen_daily->min_early_time;
							$d_Min_late_time__      = $absen_daily->min_late_time;
							$d_Min_least_time__     = $absen_daily->min_least_time;
							$d_Min_over_time_awal__ = $absen_daily->min_over_time_awal;
							$d_Min_over_time__      = $absen_daily->min_over_time;
							$d_Kode_ket__           = $absen_daily->kode_ket;
							$is_Masuk__             = $absen_daily->kode_ket == Validasi::MASUK;
							$is_Luarkota__          = $absen_daily->kode_ket == Validasi::LUARKOTA;
							$is_Sakit__             = $absen_daily->kode_ket == Validasi::SAKIT;
							$is_Koreksi__           = $absen_daily->kode_ket == Validasi::KOREKSI;
							$is_Off__               = $absen_daily->kode_ket == Validasi::OFF;
							$is_Cutitahunan__       = $absen_daily->kode_ket == Validasi::CUTITAHUNAN;
							$is_Cutimenikah__       = $absen_daily->kode_ket == Validasi::CUTIMENIKAH;
							$is_Cutibersalin__      = $absen_daily->kode_ket == Validasi::CUTIBERSALIN;
							$is_Cutiistimewa__      = $absen_daily->kode_ket == Validasi::CUTIISTIMEWA;
							$is_Cutinonaktif__      = $absen_daily->kode_ket == Validasi::CUTINONAKTIF;
							$is_Cutipresentasi__    = $absen_daily->kode_ket == Validasi::PRESENTASI;
							/** @var MasterGaji[] $masterGaji */
							$masterGaji = MasterGaji::model()->findAllByAttributes( [
								'leveling_id' => $this->leveling_id,
								'golongan_id' => $this->golongan_id,
								'area_id'     => $this->cabang->area_id,
								'bu_id'       => $this->cabang->bu_id,
							] );
							$phpCodeD   = '';
							foreach ( $masterGaji as $md ) {
								$phpCodeD .= '$' . $md->master->kode . '=' . $md->amount . ';' . PHP_EOL;
							}
							file_put_contents( $tmpfnamemasterD, "<?php" . PHP_EOL . $phpCodeD );
							include $tmpfnamemasterD;
							## END SETTING VARIABLE
							$hasilFormula__ = 0;
							file_put_contents( $tmpscdfname, "<?php" . PHP_EOL . $sc->formula );
							include $tmpscdfname;
							PayrollWorksheet::create( $periode->periode_id, $this->pegawai_id, $sc->schema_id,
								PayrollWorksheet::SCHEMA_DAILY, $hasilFormula__ );
						}
					}
				}
			}
			if ( file_exists( $tmpscdfname ) ) {
				unlink( $tmpscdfname );
			}
			if ( file_exists( $tmpfnamemasterD ) ) {
				unlink( $tmpfnamemasterD );
			}
			### END DAILY
			### MONTHLY
			$masterGaji = Yii::app()->db->createCommand( "SELECT * FROM pbu_master_gaji_view 
                WHERE pegawai_id = :pegawai_id" )
			                            ->queryAll( true, [ ':pegawai_id' => $this->pegawai_id ] );
			foreach ( $masterGaji as $md ) {
				$phpCode .= '$' . $md['mkode'] . '=' . $md['amount'] . ';' . PHP_EOL;
			}
			$tmpfnamemaster = tempnam( $dirName, 'tpay' ) . 'php';
			file_put_contents( $tmpfnamemaster, "<?php" . PHP_EOL . $phpCode );
			include $tmpfnamemaster;
			$tmpscfname = tempnam( $dirName, 'tscpay' ) . '.php';
			/** @var SchemaGajiView[] $schema */
			$schema = SchemaGajiView::model()->findAllByAttributes( [
				'status_id'        => $this->status_id,
				'jenis_periode_id' => $this->jenis_periode_id,
				'rotation'         => 'M'
			] );
			if ( $schema != null ) {
				foreach ( $schema as $sc ) {
					$hasilFormula__ = 0;
					file_put_contents( $tmpscfname, "<?php" . PHP_EOL . $sc->formula );
					include $tmpscfname;
					PayrollWorksheet::create( $periode->periode_id, $this->pegawai_id, $sc->schema_id,
						PayrollWorksheet::SCHEMA_GAJI, $hasilFormula__ );
					## Add Value ##
					/** @var AddValue $add */
					$add = AddValue::model()->findByAttributes( [
						'pegawai_id' => $this->pegawai_id,
						'schema_id'  => $sc->schema_id
					] );
					if ( $add != null ) {
						PayrollWorksheet::create( $periode->periode_id, $this->pegawai_id, $sc->schema_id,
							PayrollWorksheet::ADDED_VALUE, $add->amount );
					}
				}
			}
			if ( file_exists( $tmpscfname ) ) {
				unlink( $tmpscfname );
			}
			### END MONTHLY
			if ( file_exists( $tmpfname ) ) {
				unlink( $tmpfname );
			}
			$transaction->commit();
			return true;
		} catch ( Exception $ex ) {
			$transaction->rollback();
			return $ex->getMessage();
		}
	}

	/** @var Periode $periode */
	public function generatePayroll( $periode ) {
		$msg = 'Data berhasil digenerate.';
//		app()->db->autoCommit = false;
		$transaction = Yii::app()->db->beginTransaction();
		try {
//			$del = Yii::app()->db->createCommand( 'DELETE FROM pbu_payroll, pbu_payroll_details
//              FROM pbu_payroll LEFT JOIN pbu_payroll_details ON pbu_payroll.payroll_id = pbu_payroll_details.payroll_id
//              WHERE pbu_payroll.periode_id = :periode_id AND pbu_payroll.pegawai_id = :pegawai_id;' );
//			$del->execute( [ ':periode_id' => $periode->periode_id, ':pegawai_id' => $this->pegawai_id ] );
			Payroll::model()->deleteAllByAttributes( [
				'periode_id' => $periode->periode_id,
				'pegawai_id' => $this->pegawai_id
			] );

			/** @var Resign $resign */
			$resign = Resign::model()->findByAttributes([
				'pegawai_id'=> $this->pegawai_id
			]);

			if($resign != null){
				if($resign->tgl <= $periode->periode_start){
					$transaction->commit();
					return true;
				}
			}

			/** @var Cabang $cab */
			$cab = Cabang::model()->findByPk( $this->cabang_id );
			if ( $cab == null ) {
				throw new Exception( 'Cabang tidak ditemukan.' );
			}
			/** @var Area $area */
			$area = Area::model()->findByPk( $cab->area_id );
			if ( $area == null ) {
				throw new Exception( 'Area tidak ditemukan.' );
			}
			/** @var Jabatan $jab */
			$jab = Jabatan::model()->findByPk( $this->jabatan_id );
			if ( $jab == null ) {
				throw new Exception( 'Jabatan tidak ditemukan.' );
			}
			if ( $this->leveling_id == null ) {
				throw new Exception( 'Leveling tidak ditemukan.' );
			}
			/** @var PayrollAbsensi $absensi */
			$absensi = PayrollAbsensi::model()->findByAttributes( [
				'periode_id' => $periode->periode_id,
				'pegawai_id' => $this->pegawai_id
			] );
			if ( $absensi == null ) {
				throw new Exception( 'Absensi tidak ditemukan.' );
			}
			$pay               = new Payroll;
			$pay->pegawai_id   = $this->pegawai_id;
			$pay->kode_gol     = $this->golongan->kode;
			$pay->nama_gol     = $this->golongan->nama;
			$pay->kode_level   = $this->leveling->kode;
			$pay->nama_level   = $this->leveling->nama;
			$pay->kode_cab     = $this->cabang->kode_cabang;
			$pay->nama_cab     = $this->cabang->nama_cabang;
			$pay->kode_area    = $area->kode;
			$pay->nama_area    = $area->nama;
			$pay->nik          = $this->nik;
			$pay->leveling_id  = $this->leveling_id;
			$pay->golongan_id  = $this->golongan_id;
			$pay->area_id      = $cab->area_id;
			$pay->nama_lengkap = $this->nama_lengkap;
			$pay->nama_jabatan = $jab->nama_jabatan;
			$pay->tgl_masuk    = $this->tgl_masuk;
			$pay->email        = $this->email;
			$pay->npwp         = $this->npwp;
			$pay->lock         = 0;
//			$pay->nama_status          = $this->statusPegawai->nama_status;
			$pay->periode_id           = $periode->periode_id;
			$pay->total_hari_kerja     = $absensi->total_hari_kerja;
			$pay->total_lk             = $absensi->total_lk;
			$pay->total_cuti_tahunan   = $absensi->total_cuti_tahunan;
			$pay->total_cuti_menikah   = $absensi->total_cuti_menikah;
			$pay->total_cuti_bersalin  = $absensi->total_cuti_bersalin;
			$pay->total_cuti_istimewa  = $absensi->total_cuti_istimewa;
			$pay->total_cuti_non_aktif = $absensi->total_cuti_non_aktif;
			$pay->total_off            = $absensi->total_off;
			$pay->total_sakit          = $absensi->total_sakit;
			$pay->total_lembur_1       = $absensi->total_lembur_1;
			$pay->total_lembur_next    = $absensi->total_lembur_next;
			$pay->jatah_off            = $absensi->jatah_off;
			$pay->less_time            = $absensi->less_time;
			if ( ! $pay->save() ) {
				throw new Exception( CHtml::errorSummary( $pay ) );
			}
			$total_income = $total_deduction = $take_home_pay = 0;
			$worksheets   = Yii::app()->db
				->createCommand()
				->select( "ppw.pegawai_id,ppw.schema_id,SUM(ppw.amount) AS total,ppw.periode_id" )
				->from( "pbu_payroll_worksheet AS ppw" )
				->group( "ppw.pegawai_id,ppw.schema_id,ppw.periode_id" )
				->where( "ppw.pegawai_id = :pegawai_id AND ppw.periode_id = :periode_id", [
					':pegawai_id' => $this->pegawai_id,
					':periode_id' => $periode->periode_id
				] )->queryAll( true );
			foreach ( $worksheets as $sc ) {
				$schema = Schema::model()->findByPk( $sc['schema_id'] );
				if ( $schema == null ) {
					throw new Exception( 'Schema not found.' );
				}
				$payDetail             = new PayrollDetails;
				$payDetail->payroll_id = $pay->payroll_id;
				$payDetail->schema_id  = $schema->schema_id;
				$payDetail->seq        = $schema->seq;
				$payDetail->nama_skema = $schema->nama_skema;
				$payDetail->type_      = $schema->type_;
				$payDetail->amount     = $sc['total'];
				if ( ! $payDetail->save() ) {
					throw new Exception( CHtml::errorSummary( $payDetail ) );
				}
				if ( $payDetail->type_ == 1 ) {
					$total_income += $payDetail->amount;
				} else {
					$total_deduction += $payDetail->amount;
				}
			}
			$take_home_pay        = $total_income - $total_deduction;
			$pay->total_income    = $total_income;
			$pay->total_deduction = $total_deduction;
			$pay->take_home_pay   = $take_home_pay;
			if ( ! $pay->save() ) {
				throw new Exception( CHtml::errorSummary( $pay ) );
			}
			$transaction->commit();
			return true;
		} catch ( Exception $ex ) {
			$transaction->rollback();
			return $ex->getMessage();
		}
	}
}