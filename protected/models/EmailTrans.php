<?php

Yii::import('application.models._base.BaseEmailTrans');

class EmailTrans extends BaseEmailTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate() {
		if ($this->email_trans_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->email_trans_id = $uuid;
		}
		return parent::beforeValidate();
	}
}