<?php

/**
 * This is the model base class for the table "{{lock}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Lock".
 *
 * Columns in table "{{lock}}" available as properties of the model,
 * followed by relations of table "{{lock}}" available as properties of the model.
 *
 * @property string $lock_id
 * @property string $periode_id
 * @property string $tdate
 * @property string $user_id
 *
 * @property Periode $periode
 */
abstract class BaseLock extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{lock}}';
	}

	public static function representingColumn() {
		return 'tdate';
	}

	public function rules() {
		return array(
			array('lock_id, periode_id, tdate', 'required'),
			array('lock_id, periode_id', 'length', 'max'=>36),
			array('user_id', 'length', 'max'=>60),
			array('user_id', 'default', 'setOnEmpty' => true, 'value' => null),
			array('lock_id, periode_id, tdate, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'periode' => array(self::BELONGS_TO, 'Periode', 'periode_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'lock_id' => Yii::t('app', 'Lock'),
			'periode_id' => Yii::t('app', 'Periode'),
			'tdate' => Yii::t('app', 'Tdate'),
			'user_id' => Yii::t('app', 'User'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('lock_id', $this->lock_id, true);
		$criteria->compare('periode_id', $this->periode_id);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('user_id', $this->user_id, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}