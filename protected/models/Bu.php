<?php

Yii::import('application.models._base.BaseBu');

class Bu extends BaseBu
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate() {
		if ($this->bu_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->bu_id = $uuid;
		}
		return parent::beforeValidate();
	}
}