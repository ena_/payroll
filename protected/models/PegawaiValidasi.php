<?php

/**
 * This is the model class for table "{{pegawai_validasi}}".
 *
 * The followings are the available columns in table '{{pegawai_validasi}}':
 * @property string $validasi_id
 * @property string $pegawai_id
 * @property string $pin_id
 * @property string $pin
 * @property string $in_time
 * @property string $out_time
 * @property integer $min_early_time
 * @property integer $min_late_time
 * @property integer $min_least_time
 * @property integer $min_over_time_awal
 * @property integer $min_over_time
 * @property string $tdate
 * @property integer $kode_ket
 * @property string $no_surat_tugas
 * @property integer $status_int
 * @property string $result_id
 * @property string $user_id
 * @property string $status_pegawai_id
 * @property string $shift_id
 * @property integer $approval_lembur
 * @property integer $real_lembur_pertama
 * @property integer $real_lembur_akhir
 * @property integer $real_lembur_hari
 * @property integer $real_less_time
 * @property string $cabang_id
 * @property string $nama_lengkap
 * @property string $leveling_id
 * @property string $golongan_id
 * @property string $jenis_periode_id
 * @property string $area_id
 * @property string $master_id
 * @property string $amount
 */
class PegawaiValidasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pegawai_validasi}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('min_early_time, min_late_time, min_least_time, min_over_time_awal, min_over_time, kode_ket, status_int, approval_lembur, real_lembur_pertama, real_lembur_akhir, real_lembur_hari, real_less_time', 'numerical', 'integerOnly'=>true),
			array('validasi_id, pegawai_id, pin_id, pin, result_id, user_id, status_pegawai_id, shift_id, leveling_id, golongan_id, jenis_periode_id, area_id, master_id', 'length', 'max'=>36),
			array('no_surat_tugas, nama_lengkap', 'length', 'max'=>100),
			array('in_time, out_time, tdate, cabang_id, amount', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('validasi_id, pegawai_id, pin_id, pin, in_time, out_time, min_early_time, min_late_time, min_least_time, min_over_time_awal, min_over_time, tdate, kode_ket, no_surat_tugas, status_int, result_id, user_id, status_pegawai_id, shift_id, approval_lembur, real_lembur_pertama, real_lembur_akhir, real_lembur_hari, real_less_time, cabang_id, nama_lengkap, leveling_id, golongan_id, jenis_periode_id, area_id, master_id, amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'validasi_id' => 'Validasi',
			'pegawai_id' => 'Pegawai',
			'pin_id' => 'Pin',
			'pin' => 'Pin',
			'in_time' => 'In Time',
			'out_time' => 'Out Time',
			'min_early_time' => 'Min Early Time',
			'min_late_time' => 'Min Late Time',
			'min_least_time' => 'Min Least Time',
			'min_over_time_awal' => 'Min Over Time Awal',
			'min_over_time' => 'Min Over Time',
			'tdate' => 'Tdate',
			'kode_ket' => 'Kode Ket',
			'no_surat_tugas' => 'No Surat Tugas',
			'status_int' => 'Status Int',
			'result_id' => 'Result',
			'user_id' => 'User',
			'status_pegawai_id' => 'Status Pegawai',
			'shift_id' => 'Shift',
			'approval_lembur' => 'Approval Lembur',
			'real_lembur_pertama' => 'Real Lembur Pertama',
			'real_lembur_akhir' => 'Real Lembur Akhir',
			'real_lembur_hari' => 'Real Lembur Hari',
			'real_less_time' => 'Real Less Time',
			'cabang_id' => 'Cabang',
			'nama_lengkap' => 'Nama Lengkap',
			'leveling_id' => 'Leveling',
			'golongan_id' => 'Golongan',
			'jenis_periode_id' => 'Jenis Periode',
			'area_id' => 'Area',
			'master_id' => 'Master',
			'amount' => 'Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('validasi_id',$this->validasi_id,true);
		$criteria->compare('pegawai_id',$this->pegawai_id,true);
		$criteria->compare('pin_id',$this->pin_id,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('in_time',$this->in_time,true);
		$criteria->compare('out_time',$this->out_time,true);
		$criteria->compare('min_early_time',$this->min_early_time);
		$criteria->compare('min_late_time',$this->min_late_time);
		$criteria->compare('min_least_time',$this->min_least_time);
		$criteria->compare('min_over_time_awal',$this->min_over_time_awal);
		$criteria->compare('min_over_time',$this->min_over_time);
		$criteria->compare('tdate',$this->tdate,true);
		$criteria->compare('kode_ket',$this->kode_ket);
		$criteria->compare('no_surat_tugas',$this->no_surat_tugas,true);
		$criteria->compare('status_int',$this->status_int);
		$criteria->compare('result_id',$this->result_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status_pegawai_id',$this->status_pegawai_id,true);
		$criteria->compare('shift_id',$this->shift_id,true);
		$criteria->compare('approval_lembur',$this->approval_lembur);
		$criteria->compare('real_lembur_pertama',$this->real_lembur_pertama);
		$criteria->compare('real_lembur_akhir',$this->real_lembur_akhir);
		$criteria->compare('real_lembur_hari',$this->real_lembur_hari);
		$criteria->compare('real_less_time',$this->real_less_time);
		$criteria->compare('cabang_id',$this->cabang_id,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('leveling_id',$this->leveling_id,true);
		$criteria->compare('golongan_id',$this->golongan_id,true);
		$criteria->compare('jenis_periode_id',$this->jenis_periode_id,true);
		$criteria->compare('area_id',$this->area_id,true);
		$criteria->compare('master_id',$this->master_id,true);
		$criteria->compare('amount',$this->amount,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PegawaiValidasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
