<?php
Yii::import( 'application.models._base.BaseValidasi' );
class Validasi extends BaseValidasi {
	const MASUK = 0;
	const LUARKOTA = 1;
	const SAKIT = 2;
	const KOREKSI = 3;
	const OFF = 4;
	const CUTITAHUNAN = 5;
	const CUTIMENIKAH = 6;
	const CUTIBERSALIN = 7;
	const CUTIISTIMEWA = 8;
	const CUTINONAKTIF = 9;
	const PRESENTASI = 10;
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
}