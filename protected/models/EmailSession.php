<?php

Yii::import('application.models._base.BaseEmailSession');

class EmailSession extends BaseEmailSession
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate() {
		if ( $this->email_session_id == null ) {
			$command        = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid           = $command->queryScalar();
			$this->email_session_id = $uuid;
		}
		return parent::beforeValidate();
	}
}