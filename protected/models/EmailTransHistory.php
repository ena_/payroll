<?php
Yii::import( 'application.models._base.BaseEmailTransHistory' );
class EmailTransHistory extends BaseEmailTransHistory {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->email_trans_history_id == null ) {
			$command                      = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                         = $command->queryScalar();
			$this->email_trans_history_id = $uuid;
		}
		if ($this->tdate == null) {
			$this->tdate = new CDbExpression('NOW()');
		}
		return parent::beforeValidate();
	}
}