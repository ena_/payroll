<?php
Yii::import( 'application.models._base.BasePeriodeJoin' );
class PeriodeJoin extends BasePeriodeJoin {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->periode_join_id == null ) {
			$command               = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                  = $command->queryScalar();
			$this->periode_join_id = $uuid;
		}
		if ( $this->periode_id == $this->periode_id_child ) {
			$this->addError( 'periode_id_child', 'Periode tidak bisa join keperiode yang sama.' );
		}
		return parent::beforeValidate();
	}
}