<?php
class PayrollAbsensiController extends GxController {
	public function actionCreate() {
//        $model = new PayrollAbsensi;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$status = false;
//            app()->db->autoCommit = false;
			$detils      = CJSON::decode( $_POST['detil'] );
			$transaction = Yii::app()->db->beginTransaction();
			try {
				foreach ( $detils as $r ) {
					PayrollAbsensi::saveData( $r['periode_id'], $r['pegawai_id'], $r['total_hari_kerja'],
						$r['total_lk'], $r['total_sakit'], $r['total_cuti_tahunan'], $r['total_off'],
						$r['total_lembur_1'], $r['total_lembur_next'], $r['jatah_off'], $r['total_cuti_menikah'],
						$r['total_cuti_bersalin'], $r['total_cuti_istimewa'], $r['total_cuti_non_aktif'],
						$r['less_time'], $r['total_presentasi'] );
				}
//                $model->save();
				$transaction->commit();
				$status = true;
				$msg    = 'Absensi Payroll berhasil disimpan.';
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
//            app()->db->autoCommit = true;
//            foreach ($_POST as $k => $v) {
//                if (is_angka($v)) $v = get_number($v);
//                $_POST['PayrollAbsensi'][$k] = $v;
//            }
//            $model->attributes = $_POST['PayrollAbsensi'];
//            $msg = "Data gagal disimpan.";
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->payroll_absensi_id;
//            } else {
//                $msg .= " " . CHtml::errorSummary($model);
//                $status = false;
//            }
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'PayrollAbsensi' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['PayrollAbsensi'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['PayrollAbsensi'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->payroll_absensi_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->payroll_absensi_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'PayrollAbsensi' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		$arr = U::absensiPayrollIndex( $_POST['periode_id'] );
		$this->renderJsonArr( $arr );
	}
	public function actionSaveLaporan() {
		$periode_id = $_POST['periode_id'];
		$bu_id      = $_POST['bu_id'];
		$transaction = Yii::app()->db->beginTransaction();
		try {
			$count  = PayrollAbsensi::tarikAbsen( $periode_id, $bu_id );
			$status = true;
			$msg    = "$count data berhasil disimpan!";
			$transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
		}
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
	}
	static function check_lock( $periode ) {
		$comm = Yii::app()->db->createCommand(
			"SELECT * FROM pbu_result WHERE periode_id = '" . $periode . "'" )
		                      ->queryScalar();
		if ( ! $comm ) {
			$msg = '<b>GAGAL!</b></br>Laporan tidak ada data di Periode ini!!!';
			echo CJSON::encode( array(
				'success' => false,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			return '';
		}
	}
}