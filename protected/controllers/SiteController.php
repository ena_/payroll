<?php
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PhpOffice\PhpWord\Settings;
Yii::import( 'application.components.U' );
Yii::import( 'application.components.SoapClientYii' );
class SiteController extends Controller {
//    public function accessRules() {
//        return array(
//            array('allow',
//                'users' => array('*'),
//                'actions' => array('login'),
//            )           
//        );
//    }
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array( 'class' => 'CViewAction', ),
		);
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = 'main';
		$this->render( 'index' );
	}
	public function actionEmail() {
		Yii::import( 'application.extensions.vendor.phpmailer.Exception' );
		Yii::import( 'application.extensions.vendor.phpmailer.PHPMailer' );
		Yii::import( 'application.extensions.vendor.phpmailer.SMTP' );
		/** @var PHPMailer $mail */
		$mail = new PHPMailer();
		$mail->isSendmail();
		$mail->Host       = 'smtp.gmail.com';
		$mail->Username   = 'ingpayroll@gmail.com';
		$mail->Password   = 'hrdindonatashagemilang';
		$mail->Mailer     = 'smtp';
		$mail->Port       = 587;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = 'tls';
		$mail->SetFrom( 'ingpayroll@gmail.com', 'PAYROLL' );
		$mail->Subject = 'SLIP TEST';
		$mail->IsHTML( true );
		$mail->MsgHTML( " Tanya jawab gaji ke ::: " . $mail->Username .
		                "<br><br>Sekian dan Terima Kassih." );
		$mail->ClearAddresses();
		$mail->ClearAttachments();
//		$mail->AddAddress( $email_tran->email );
		$mail->AddAddress( 'novebeta@gmail.com' );
		echo "\t >> sending..... \n";
		$status = 0;
		$msg    = '';
		if ( ! $mail->Send() ) {
			$msg    = $mail->ErrorInfo;
			$status = 1;
		} else {
			$msg    = "OK";
			$status = 0;
		}
		echo "\t >> Message : $msg \n";
	}
	public function actionEvalMath() {
		Yii::import( 'application.components.EvalMath' );
		$this->layout = 'plain';
		$periode      = 'e9d36216-41ca-11e7-a778-1e5656ac5cc8';
		/** @var Pegawai $allPegawai */
		$allPegawai = Pegawai::model()->find();
		$phpCode    = '$HK__= 25;' . PHP_EOL;
		$phpCode    .= '$LK__= 2;' . PHP_EOL;
		$phpCode    .= '$CT__= 1;' . PHP_EOL;
		$phpCode    .= '$OFF__= 4;' . PHP_EOL;
		$phpCode    .= '$SICK__= 2;' . PHP_EOL;
//        $gol = Golongan::model()->find();
//        if ($gol == null) {
//            throw new Exception('Master Golongan harus diinput dulu.');
//        }
//        $mGaji = [];
		$masterGaji = Yii::app()->db
			->createCommand( "SELECT * FROM pbu_master_gaji_view WHERE pegawai_id = :pegawai_id" )
			->queryAll( true, [ ':pegawai_id' => $allPegawai ] );
//        foreach ($masterGaji as $row) {
//            $mGaji[$row['master_id']][$row['leveling_id']][$row['golongan_id']] = $row['amount'];
//        }
		/** @var Master[] $master */
		$master = Master::model()->findAll();
		foreach ( $master as $md1 ) {
			$phpCode .= '$' . $md1->kode . '=0;' . PHP_EOL;
		}
		foreach ( $masterGaji as $md ) {
			$phpCode .= '$' . $md['mkode'] . '=' . $md['amount'] . ';' . PHP_EOL;
		}
		$formula = '$hasilFormula__ = 0;';
		$formula = $phpCode . ' ' . $formula;
		var_dump( $formula );
		$dirName  = Yii::getPathOfAlias( 'application.runtime.' . Yii::app()->controller->id );
		$tmpfname = tempnam( $dirName, 'tpay' );
		$handle   = fopen( $tmpfname, "w+" );
		fwrite( $handle, "<?php\n" . $formula );
		fclose( $handle );
		try {
			include $tmpfname;
		} catch ( Exception $e ) {
			unlink( $tmpfname );
			echo 'Require failed! Error: ' . $e;
		}
		unlink( $tmpfname );
		if ( ! isset( $hasilFormula__ ) ) {
			echo 'Variable $hasilFormula__ harus diset';
		}
		if ( is_angka( $hasilFormula__ ) === false ) {
			echo 'Hasil $hasilFormula__ harus numeric [' . $hasilFormula__ . ']';
		}
	}

//    public function actionEvalMath()
//    {
//        Yii::import('application.components.EvalMath');
//        $this->layout = 'plain';
//        $periode = 'e9d36216-41ca-11e7-a778-1e5656ac5cc8';
//        /** @var Pegawai[] $allPegawai */
//        $allPegawai = Pegawai::model()->findAll();
//        $mGaji = [];
//        $masterGaji = Yii::app()->db->createCommand("SELECT * FROM pbu_master_gaji_view")
//            ->queryAll(true);
//        foreach ($masterGaji as $row) {
//            $mGaji[$row['master_id']][$row['leveling_id']][$row['golongan_id']] = $row['amount'];
//        }
////        var_dump($mGaji);
//        $dirName = Yii::getPathOfAlias('application.runtime.' . Yii::app()->controller->id);
//        $tmpfname = tempnam($dirName, 'tpay');
//        foreach ($allPegawai as $peg) {
//            $handle = fopen($tmpfname, "w+");
//            $phpCode = '';
//            $gol_id = Golongan::model()->findByAttributes([
//                'golongan_id' => $peg->golongan_id
//            ]);
//            if ($gol_id == null) {
//                continue;
//            }
//            $absensi = PayrollAbsensi::model()->findByAttributes([
//                'periode_id' => $periode,
//                'pegawai_id' => $peg->pegawai_id
//            ]);
//            if ($absensi == null) {
//                continue;
//            }
//            $phpCode .= '$HK__=' . $absensi->total_hari_kerja . ';' . PHP_EOL;
//            $phpCode .= '$LK__=' . $absensi->total_lk . ';' . PHP_EOL;
//            $phpCode .= '$CT__=' . $absensi->total_cuti_tahunan . ';' . PHP_EOL;
//            $phpCode .= '$OFF__=' . $absensi->total_off . ';' . PHP_EOL;
//            $phpCode .= '$SICK__=' . $absensi->total_sakit . ';' . PHP_EOL;
//            /** @var Master[] $master */
//            $master = Master::model()->findAll();
//            foreach ($master as $md) {                
//                $phpCode .= '$' . $md->kode . '=' . $mGaji[$md->master_id][$peg->leveling_id][$peg->golongan_id] . ';' . PHP_EOL;
//            }
//            $phpCode .= '$value=$GP;' . PHP_EOL;
//            $length = 10;
//            fwrite($handle, "<?php" . PHP_EOL . $phpCode);
//            fclose($handle);
//            include $tmpfname;
//            $tmpscfname = tempnam($dirName, 'tscpay');
//            $schema = SchemaGaji::model()->findAll();
//            foreach ($schema as $sc) {
//                $hasilFormula__ = 0;
//                $handlesc = fopen($tmpscfname, "w+");
//                fwrite($handlesc, "<?php" . PHP_EOL . $sc->formula);
//                fclose($handlesc);
//                include $tmpscfname;
////                $variable = get_defined_vars();
//                var_dump($hasilFormula__);
//            }
//            unlink($tmpscfname);
//        }
//        unlink($tmpfname);
//        Yii::app()->end();
//    }
	public function actionGetDateTime() {
		if ( Yii::app()->request->isAjaxRequest ) {
			echo CJSON::encode( array(
				'success'  => true,
				'datetime' => date( 'Y-m-d H:i:s' )
			) );
			Yii::app()->end();
		}
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest ) {
				echo json_encode( var_export( $error ) );
			} //$error['message'];
			else {
				$this->render( 'error', $error );
			}
		}
		var_dump( get_defined_vars() );
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if ( isset( $_POST['ContactForm'] ) ) {
			$model->attributes = $_POST['ContactForm'];
			if ( $model->validate() ) {
				$name    = '=?UTF-8?B?' . base64_encode( $model->name ) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode( $model->subject ) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
				           "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
				mail( Yii::app()->params['adminEmail'], $subject, $model->body, $headers );
				Yii::app()->user->setFlash( 'contact', 'Thank you for contacting us. We will respond to you as soon as possible.' );
				$this->refresh();
			}
		}
		$this->render( 'contact', array( 'model' => $model ) );
	}

	/**
	 * Displays the login page
	 */
	//  public function actionLogin()
//    {
//        $model = new LoginForm;
//
//        // if it is ajax validation request
//        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form')
//        {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//
//        // collect user input data
//        if (isset($_POST['LoginForm']))
//        {
//            $model->attributes = $_POST['LoginForm'];
//            // validate user input and redirect to the previous page if valid
//            if ($model->validate() && $model->login())
//                $this->redirect(Yii::app()->user->returnUrl);
//        }
//        // display the login form
//        $this->render('login', array('model' => $model));
//    }
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect( Yii::app()->homeUrl );
	}
	public function actionLogin() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->layout = 'login';
			$this->render( 'login' );
		} else {
			$model         = new LoginForm;
			$loginUsername = isset( $_POST["loginUsername"] ) ? $_POST["loginUsername"] : "";
			$loginPassword = isset( $_POST["loginPassword"] ) ? $_POST["loginPassword"] : "";
			if ( $loginUsername != "" ) {
				//$model->attributes = $_POST['LoginForm'];
				$model->username = $loginUsername;
				$model->password = $loginPassword;
				// validate user input and redirect to the previous page if valid
				if ( $model->validate() && $model->login() ) {
					echo "{success: true}";
				} else {
					echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
				}
			} else {
				echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
			}
		}
	}
	public function actionBackupAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = dirname( Yii::app()->request->scriptFile ) . "\\backup\\";
		$files      = scandir( $dir_backup );
		foreach ( $files as $file ) {
			if ( is_file( "$dir_backup\\$file" ) ) {
				unlink( "$dir_backup\\$file" );
			}
		}
		$backup_file = $dir_backup . $db . date( "Y-m-d-H-i-s" ) . '.pos';
		$mysqldump   = MYSQLDUMP;
//        $gzip = GZIP;
		$command = "$mysqldump --opt -h $host -u $user --password=$pass -P $port " .
		           "$db > $backup_file";
		system( $command );
//        $size = filesize($backup_file);
//        if ($size > 0) {
//            $command = "$gzip -9 $backup_file";
//            system($command);
//            $size = filesize("$backup_file.gz");
//            $bu = bu() . "/backup/" . basename("$backup_file.gz");
//            header("Location: $bu");
//        }
	}
	public function actionRestoreAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = dirname( Yii::app()->request->scriptFile ) . "\\backup\\";
		if ( isset( $_FILES["filename"] ) ) { // it is recommended to check file type and size here
			if ( $_FILES["filename"]["error"] > 0 ) {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => $_FILES["file"]["error"]
				) );
			} else {
				$backup_file = $dir_backup . $_FILES["filename"]["name"];
				move_uploaded_file( $_FILES["filename"]["tmp_name"], $backup_file );
				$mysql   = MYSQL;
				$gzip    = GZIP;
				$command = "$gzip -d $backup_file";
				system( $command );
				$backup_file = substr( $backup_file, 0, - 3 );
				if ( ! file_exists( $backup_file ) ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "Failed restore file " . $_FILES["file"]["name"]
					) );
				} else {
					Yii::app()->db->createCommand( "DROP DATABASE $db" )
					              ->execute();
					Yii::app()->db->createCommand( "CREATE DATABASE IF NOT EXISTS $db" )
					              ->execute();
					$command = "$mysql -h $host -u $user --password=$pass -P $port " .
					           "$db < $backup_file";
					system( $command );
					Yii::app()->db->createCommand( "USE $db" )
					              ->execute();
					echo CJSON::encode( array(
						'success' => true,
						'msg'     => "Succefully restore file " . $_FILES["filename"]["name"]
					) );
				}
				Yii::app()->end();
			}
		}
	}
	public function actionDeleteTransAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		Yii::app()->db->createCommand( "
        /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
        TRUNCATE TABLE pbu_credit_note_details;
        TRUNCATE TABLE pbu_credit_note;
        TRUNCATE TABLE pbu_refs;
        TRUNCATE TABLE pbu_sj_details;
        TRUNCATE TABLE pbu_sj;
        TRUNCATE TABLE pbu_sjint_details;
        TRUNCATE TABLE pbu_sjint;
        TRUNCATE TABLE pbu_sjtax_details;
        TRUNCATE TABLE pbu_sjtax;
        TRUNCATE TABLE pbu_sjsim_details;
        TRUNCATE TABLE pbu_sjsim;
        TRUNCATE TABLE pbu_stock_moves;
        TRUNCATE TABLE pbu_terima_barang_details;
        TRUNCATE TABLE pbu_terima_barang;
        /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
        " )->execute();
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => "Succefully delete all transaction "
		) );
	}
	public function actionGenerate() {
		$templatePath = './css/silk_v013/icons';
		$files        = scandir( $templatePath );
		$txt          = "";
		foreach ( $files as $file ) {
			if ( is_file( $templatePath . '/' . $file ) ) {
				$basename = explode( ".", $file );
				$name     = $basename[0];
				$txt      .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
			}
		}
		$myFile = "silk013.css";
		$fh = fopen( $myFile, 'w' ) or die( "can't open file" );
		fwrite( $fh, $txt );
		fclose( $fh );
	}
	public function actionTree() {
		$user = Users::model()->findByPk( user()->getId() );
		$menu = new MenuTree( $user->security_roles_id );
		$data = $menu->get_menu();
//        $data = "[{text: 'Penjualan',
//                  expanded: false,
//                  children:[{
//                  text: 'Faktur Penjualan',
//                  id: 'jun.NotaGrid',
//                  leaf: true
//                  },
//                  {
//                  text: 'Sales dan Pelanggan',
//                  expanded: false,
//                  children:[{
//                    text: 'Managemen Pelanggan',
//                    id: 'jun.CustomersGrid',
//                    leaf: true
//                    }]
//                  }]
//                  },
//                  {text: 'Pembelian',
//                  expanded: false,
//                  children:[{
//                    text: 'Pembelian Kredit',
//                    id: 'jun.NotaGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Pembelian Tunai',
//                    id: 'jun.NotaGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Supplier',
//                    expanded: false,
//                    children:[{
//                        text: 'Managemen Supplier',
//                        id: 'jun.SuppliersGrid',
//                        leaf: true
//                        }]
//                    }]
//                  },
//                  {text: 'Item',
//                  expanded: false,
//                  children:[{
//                    text: 'Item',
//                    id: 'jun.StockMasterGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Satuan Item',
//                    id: 'jun.ItemUnitsGrid',
//                    leaf: true
//                    },
//                    {
//                    text: 'Kategori Stok',
//                    id: 'jun.StockCategoryGrid',
//                    leaf: true
//                    }]
//                  },
//                  {text: 'Kas/Bank',
//                    expanded: false,
//                    children:[{
//                        text: 'Kas Keluar',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                        text: 'Kas Masuk',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                        text: 'Transfer Antar Bank',
//                        id: 'jun.TranferBankWin',
//                        leaf: true
//                        },{
//                        text: 'Penyesuain Bank',
//                        id: 'jun.NotaGrid',
//                        leaf: true
//                        },{
//                         text: 'Bank Statement',
//                            id: 'jun.BankTransGrid',
//                           leaf: true
//                        },
//                        {
//                        text: 'Managemen',
//                        expanded: false,
//                        children:[{
//                            text: 'Akun Bank',
//                            id: 'jun.BankAccountsGrid',
//                            leaf: true
//                            }]
//                        }]},
//                  {
//                    text:'Akuntansi',
//                    expanded: false,
//                    children:[{
//                    text: 'Input Jurnal',
//                    id: 'jun.TblUserGrid',
//                    leaf: true
//                    },{
//                    text: 'GL Inquiry',
//                    id: 'jun.GlTransGrid',
//                    leaf: true
//                    },{
//                        text: 'Managemen',
//                        expanded: false,
//                        children:[{
//                            text: 'Quick Entries',
//                            id: 'jun.NotaGrid',
//                            leaf: true
//                            },{
//                            text: 'Akun Rekening',
//                            id: 'jun.ChartMasterGrid',
//                            leaf: true
//                            },{
//                            text: 'Grup Akun Rekening',
//                            id: 'jun.ChartTypesGrid',
//                            leaf: true
//                            },{
//                            text: 'Kelas Akun Rekening',
//                            id: 'jun.ChartClassGrid',
//                            leaf: true
//                            }]
//                        }
//                    ]},
//                    {
//                                text: 'Setting',
//                                expanded: false,
//                                children:[{
//                                    text: 'Forms Setup',
//                                    id: 'jun.SysTypesGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Profile',
//                                    id: 'jun.KlasfikasiDetailManfaatGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Ubah Password',
//                                    id: 'jun.KlasfikasiDetailManfaatGrid',
//                                    leaf: true
//                                },{
//                                    text: 'Users',
//                                    id: 'jun.UsersGrid',
//                                    leaf: true
//                                }]
//
//                            }]";
		Yii::app()->end( $data );
	}
	public function actionTest() {
//		$client = new SoapClientYii();
//		$id     = array( 'id' => '1' );
//		echo $client->getQuery( $id );
		echo sql2date( '2017-12-28', 'ddMMyy' );
	}
	public function actionCoba() {
		$pdf = Yii::createComponent( 'application.extensions.tcpdf.ETcPdf',
			'P', 'cm', 'A4', true, 'UTF-8' );
		$pdf->SetCreator( PDF_CREATOR );
		// set document information
//        $pdf->SetCreator(PDF_CREATOR);
	}
	public function actionExcel() {
		$spreadsheet = new Spreadsheet();
		$sheet       = $spreadsheet->getActiveSheet();
		$sheet->setCellValue( 'A1', 'Hello World !' );
		$writer = new Xlsx( $spreadsheet );
		$writer->save( 'hello world.xlsx' );
	}
	public function actionWord() {
		$T1         = 200;
		$T1_1       = 500;
		$T2         = 1600;
		$T2_1       = 3000;
		$T2_2       = 3200;
		$T3         = 6500;
		$T3_1       = 7000;
		$T4         = 6600;
		$T5         = 10500;
		$T5_1       = 10700;
		$T6         = 13000;
		$domPdfPath = realpath( PHPWORD_BASE_DIR . '/../../../vendor/dompdf/dompdf' );
		$phpWord    = new \PhpOffice\PhpWord\PhpWord();
		Settings::setPdfRendererPath( $domPdfPath );
		Settings::setPdfRendererName( Settings::PDF_RENDERER_DOMPDF );
		$fontStyleTitle = new \PhpOffice\PhpWord\Style\Font();
		$fontStyleTitle->setBold( true );
		$fontStyleTitle->setName( 'Calibri' );
		$fontStyleTitle->setSize( 11 );
		$fontBody = new \PhpOffice\PhpWord\Style\Font();
		$fontBody->setName( 'Calibri' );
		$fontBody->setSize( 11 );
		$multipleTabsStyleName = 'multipleTab';
		$phpWord->addParagraphStyle(
			$multipleTabsStyleName,
			array(
				'tabs' => array(
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T1 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T2 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T3 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T4 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T5 ),
				),
			)
		);
		$bodyTab = 'bodyTab';
		$phpWord->addParagraphStyle(
			$bodyTab,
			array(
				'tabs' => array(
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T1 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_1 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_2 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T3 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T5 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T5_1 ),
				),
			)
		);
		$section = $phpWord->addSection( [
			'orientation' => 'landscape'
		] );
		$section->addImage( 'images/logo_slip.png',
			array(
				'positioning'   => 'relative',
				'width'         => 310.08,
				'height'        => 75.84,
				'alignment'     => \PhpOffice\PhpWord\SimpleType\Jc::START,
				'wrappingStyle' => 'behind'
			) );
		$textrun = $section->addTextRun( [
			'tabs' => [ new \PhpOffice\PhpWord\Style\Tab( 'left', $T3 ) ]
		] );
		$textrun->addText( "\t" );
		$textrun->addText( "SLIP GAJI KARYAWAN",
			[ 'bold' => true, 'underline' => 'solid', 'size' => 12, 'name' => 'Calibri' ] );
		$section->addText( "\t\t\tPERIODE\t:\tJANUARI 2018",
			[ 'size' => 10, 'name' => 'Calibri' ],
			$multipleTabsStyleName );
		$section->addText( "\t\t\tCABANG\t:\tJOG03",
			[ 'size' => 10, 'name' => 'Calibri' ],
			$multipleTabsStyleName );
		$section->addText( '' );
		$section->addText( '' );
		$section->addText( '' );
		$section->addText( "\tNIK\t:\tJABATAN\t:\t", $fontStyleTitle, $multipleTabsStyleName );
		$section->addText( "\tNAMA\t:\tGRADE\t:\t", $fontStyleTitle, $multipleTabsStyleName );
		$section->addText( '' );
		$section->addText( '' );
		$section->addText( "\tPENDAPATAN\tPOTONGAN", $fontStyleTitle, [
			'tabs' => [
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T1_1 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T3_1 )
			]
		] );
		$section->addText( "\tGaji Pokok\t:\tRp \tKas Bon\t:\tRp ", $fontBody, $bodyTab );
		$section->addText( "\tTotal Pendapatan\t:\tRp \tTotal Potongan\t:\tRp", [
			'bold' => true,
			'size' => 11,
			'name' => 'Calibri'
		], [
			'tabs' => [
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T1_1 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_1 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_2 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T3 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T5 ),
				new \PhpOffice\PhpWord\Style\Tab( 'left', $T5_1 ),
			]
		] );
		$section->addText( "\tTAKE HOME PAY\t:\tRp ", [
			'bold' => true,
			'size' => 12,
			'name' => 'Calibri'
		],
			[
				'tabs' => [
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T1 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_1 ),
					new \PhpOffice\PhpWord\Style\Tab( 'left', $T2_2 ),
				]
			] );
//		$xmlWriter   = \PhpOffice\PhpWord\IOFactory::createWriter( $phpWord, 'Word2007' );
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter( $phpWord, 'PDF' );
//		$contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
		$contentType = 'Content-type: application/pdf;';
		header( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
		header( "Last-Modified: " . gmdate( "D,d M YH:i:s" ) . " GMT" );
		header( "Cache-Control: no-cache, must-revalidate" );
		header( "Pragma: no-cache" );
		header( $contentType );
		header( "Content-Disposition: attachment; filename=" . 'result.pdf' );
		$xmlWriter->save( "php://output" );
	}
}
