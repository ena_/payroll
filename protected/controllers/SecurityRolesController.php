<?php
class SecurityRolesController extends GxController {
	public function actionCreate() {
		$transaction = Yii::app()->db->beginTransaction();
		$msg         = "Data berhasil di simpan.";
		try {
			$model = new SecurityRoles;
			if ( ! Yii::app()->request->isAjaxRequest ) {
				return;
			}
			if ( isset( $_POST ) && ! empty( $_POST ) ) {
				$section = array();
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $k ) ) {
						$section[] = $k;
						continue;
					}
					$_POST['SecurityRoles'][ $k ] = $v;
				}
				$_POST['SecurityRoles']['sections'] = implode( ",", $section );
				$model->attributes                  = $_POST['SecurityRoles'];
				$msg                                = t( 'save.fail', 'app' );
				if ( ! $model->save() ) {
					throw new Exception( CHtml::errorSummary( $model ) );
				}
				$cabang = CJSON::decode( $_POST['cabang'] );
				$level  = CJSON::decode( $_POST['level'] );
				foreach ( $cabang as $d ) {
					if ( $d['checked'] ) {
						$c                    = new SrCabang;
						$c->cabang_id         = $d['cabang_id'];
						$c->security_roles_id = $model->security_roles_id;
						if ( ! $c->save() ) {
							throw new Exception( CHtml::errorSummary( $c ) );
						}
					}
				}
				foreach ( $level as $d ) {
					if ( $d['checked'] ) {
						$c                    = new SrLeveling();
						$c->leveling_id       = $d['leveling_id'];
						$c->security_roles_id = $model->security_roles_id;
						if ( ! $c->save() ) {
							throw new Exception( CHtml::errorSummary( $c ) );
						}
					}
				}
				$status = true;
				$transaction->commit();
			}
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
		}
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
	}
	public function actionUpdate( $id ) {
		$transaction = Yii::app()->db->beginTransaction();
		$msg = "Data berhasil di simpan.";
		try {
			$model = $this->loadModel( $id, 'SecurityRoles' );
			if ( isset( $_POST ) && ! empty( $_POST ) ) {
				$section = array();
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $k ) ) {
						$section[] = $k;
						continue;
					}
					$_POST['SecurityRoles'][ $k ] = $v;
				}
				$msg                                = t( 'save.fail', 'app' );
				$_POST['SecurityRoles']['sections'] = implode( ",", $section );
				$model->attributes                  = $_POST['SecurityRoles'];
				if ( ! $model->save() ) {
					throw new Exception( CHtml::errorSummary( $model ) );
				}
				SrLeveling::model()->deleteAllByAttributes( [ 'security_roles_id' => $model->security_roles_id ] );
				SrCabang::model()->deleteAllByAttributes( [ 'security_roles_id' => $model->security_roles_id ] );
				$cabang = CJSON::decode( $_POST['cabang'] );
				$level  = CJSON::decode( $_POST['level'] );
				foreach ( $cabang as $d ) {
					if ( $d['checked'] ) {
						$c                    = new SrCabang;
						$c->cabang_id         = $d['cabang_id'];
						$c->security_roles_id = $model->security_roles_id;
						if ( ! $c->save() ) {
							throw new Exception( CHtml::errorSummary( $c ) );
						}
					}
				}
				foreach ( $level as $d ) {
					if ( $d['checked'] ) {
						$c                    = new SrLeveling();
						$c->leveling_id       = $d['leveling_id'];
						$c->security_roles_id = $model->security_roles_id;
						if ( ! $c->save() ) {
							throw new Exception( CHtml::errorSummary( $c ) );
						}
					}
				}
				$status = true;
				$transaction->commit();
//				if ( Yii::app()->request->isAjaxRequest ) {
//					echo CJSON::encode( array(
//						'success' => $status,
//						'msg'     => $msg
//					) );
//					Yii::app()->end();
//				} else {
//					$this->redirect( array( 'view', 'id' => $model->security_roles_id ) );
//				}
			}
		} catch ( Exception $ex ) {
			$transaction->rollback();
			$status = false;
			$msg    = $ex->getMessage();
		}
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'SecurityRoles' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) )
		) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = SecurityRoles::model()->findAll( $criteria );
		$total = SecurityRoles::model()->count( $criteria );
		foreach ( $model AS $dodol ) {
			$row      = $dodol->getAttributes();
			$sections = explode( ',', $row['sections'] );
			foreach ( $sections as $line ) {
				$row[ $line ] = 1;
			}
			$argh[] = $row;
		};
		$jsonresult = '{"total":"' . $total . '","results":' . json_encode( $argh ) . '}';
		Yii::app()->end( $jsonresult );
	}
}