<?php
class LevelingController extends GxController
{
    public function actionCreate()
    {
        $model = new Leveling;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Leveling'][$k] = $v;
            }
            $model->attributes = $_POST['Leveling'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->leveling_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Leveling');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Leveling'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Leveling'];
            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->leveling_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->leveling_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Leveling')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $params = [];
        $id = Yii::app()->user->getId();
        $sri = Users::model()
            ->findByAttributes(array('id' => $id))
            ->security_roles_id;
        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->alias = 'pp';
        $criteria->join = 'INNER JOIN pbu_sr_leveling as sl ON pp.leveling_id = sl.leveling_id';
        $criteria->condition = 'sl.security_roles_id = :security_roles_id';
        $params[':security_roles_id'] = $sri;
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('pp.bu_id = :bu_id');
            $params[':bu_id'] = $_POST['bu_id'];
        }
        $criteria->params = $params;
        $model = Leveling::model()->findAll($criteria);
        $total = Leveling::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexSr()
    {
        $criteria = new CDbCriteria();
        $param = [];
        if ($_POST['modez'] === '0') {
            $criteria->params = $param;
            $model = AllLvlBu::model()->findAll($criteria);
            $total = AllLvlBu::model()->count($criteria);
            $this->renderJson($model, $total);
        } else {
            $_POST['security_roles_id'] = $_POST['modez'];
            $on = " AND security_roles_id = :security_roles_id";
            $param[':security_roles_id'] = $_POST['security_roles_id'];
            $comm = Yii::app()->db->createCommand("
            SELECT 	c.kode,	c.nama,	c.bu_name,
            CASE WHEN sr.sr_leveling IS NULL THEN  0 ELSE  1 END AS checked, sr.sr_leveling,c.leveling_id
            FROM pbu_all_lvl_bu AS c
            LEFT JOIN pbu_sr_leveling AS sr ON sr.leveling_id = c.leveling_id
            $on ");
            $arr = $comm->queryAll(true, $param);
            $this->renderJsonArr($arr);
            if (isset($_POST['security_roles_id'])) {
                $criteria->addCondition("security_roles_id = :security_roles_id");
                $param[':security_roles_id'] = $_POST['security_roles_id'];
            }
            $criteria->params = $param;
            $model = SrLevelBu::model()->findAll($criteria);
            $total = SrLevelBu::model()->count($criteria);
            $this->renderJson($model, $total);
        }
    }
    public function actionSinkron()
    {
        $html = "";
        $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllLeveling',
            http_build_query(['bu_id' => $_POST['bu_id']]));
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $model = Leveling::model()->findByPk($result['lvljabatan_id']);
            if ($model == null) {
                $model = new Leveling;
                $model->leveling_id = $result['lvljabatan_id'];
            }
	        $model->bu_id = $result['bu_id'];
	        $model->kode  = $result['urutan_romawi'];
	        $model->nama  = $result['nama_level'];
//            $model->setAttributes($result);
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
}
