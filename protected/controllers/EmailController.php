<?php
class EmailController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$user_id = Yii::app()->user->id;
			/** @var Users $user */
			$user  = Users::model()->findByPk( $user_id );
			$model = $user->email;
			if ( $model == null ) {
				$model = new Email;
			}
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Email'][ $k ] = $v;
			}
			$model->attributes = $_POST['Email'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->email_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Email' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Email'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Email'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->email_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->email_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Email' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', /** @lang text */
					'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionToken() {
		$client = new Google_Client();
		$client->setApplicationName( 'Payroll Gmail API PHP Quickstart' );
		$client->setScopes( Google_Service_Gmail::GMAIL_COMPOSE );
//		$client->setAuthConfig( YiiBase::getPathOfAlias( 'application' ) . '/credentials.json' );
		$client->setAuthConfig( CJSON::decode( $_POST['credentials'], true ) );
		$client->setAccessType( 'offline' );
		$client->setPrompt( 'select_account consent' );
		if ( ! isset( $_POST['token'] ) ) {
			$authUrl = $client->createAuthUrl();
			$msg     = '<a href="' . $authUrl . '" target="_blank">' . $authUrl . '</a><br/>Enter verification code: ';
		} else {
			$accessToken = $client->fetchAccessTokenWithAuthCode( $_POST['token'] );
			$client->setAccessToken( $accessToken );
//			$client->setAccessToken( $accessToken );
			// Check to see if there was an error.
			if ( array_key_exists( 'error', $accessToken ) ) {
				$msg = join( ', ', $accessToken );
			} else {
				$msg = CJSON::encode( $client->getAccessToken() );
			}
		}
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => $msg
		) );
		Yii::app()->end();
	}
	public function actionIndex() {
//		if ( isset( $_POST['limit'] ) ) {
//			$limit = $_POST['limit'];
//		} else {
//			$limit = 20;
//		}
//		if ( isset( $_POST['start'] ) ) {
//			$start = $_POST['start'];
//		} else {
//			$start = 0;
//		}
		$criteria = new CDbCriteria();
		/** @var Users $user */
//		$user = Users::model()->findByPk( Yii::app()->user->id );
		$user = Users::model()->findByPk( $_POST['id'] );
		$criteria->addCondition( 'email_id = :email_id' );
		if ( $user->email != null ) {
			$criteria->params[':email_id'] = $user->email_id;
		} else {
			$criteria->params[':email_id'] = $_POST['id'];
		}
//		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
//		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
//			$criteria->limit  = $limit;
//			$criteria->offset = $start;
//		}
		$model = Email::model()->findAll( $criteria );
		$total = Email::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}