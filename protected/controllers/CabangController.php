<?php
class CabangController extends GxController
{
    public function actionCreate()
    {
        $model = new Cabang;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Cabang'][$k] = $v;
            }
            $model->attributes = $_POST['Cabang'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cabang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Cabang');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Cabang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Cabang'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->cabang_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->cabang_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Cabang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $params = [];
        $id = Yii::app()->user->getId();
        $sri = Users::model()
            ->findByAttributes(array('id' => $id))
            ->security_roles_id;
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
//        if (isset ($_POST['f']) && $_POST['f'] == 'usr') {
//            $cabang = Cabang::model()->checkCabang();
//            if ($cabang != 'ALL') {
//                $criteria->addCondition("kode_cabang = '$cabang'");
//            }
//        }
        $criteria->alias = 'pp';
        $criteria->join = 'INNER JOIN pbu_sr_cabang as sl ON pp.cabang_id = sl.cabang_id';
        $criteria->condition = 'sl.security_roles_id = :security_roles_id';
        $params[':security_roles_id'] = $sri;
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('pp.bu_id = :bu_id');
            $params[':bu_id'] = $_POST['bu_id'];
        }
        $criteria->params = $params;
        $model = Cabang::model()->findAll($criteria);
        $total = Cabang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexSrCabang()
    {
        $criteria = new CDbCriteria();
        $param = [];
        if ($_POST['modez'] === '0') {
            $criteria->params = $param;
            $model = AllCbgBu::model()->findAll($criteria);
            $total = AllCbgBu::model()->count($criteria);
            $this->renderJson($model, $total);
        } else {
            $_POST['security_roles_id'] = $_POST['modez'];
            $on = " AND security_roles_id = :security_roles_id";
            $param[':security_roles_id'] = $_POST['security_roles_id'];
            $comm = Yii::app()->db->createCommand("
            SELECT 	c.kode_cabang,	c.nama_cabang,	c.bu_name,c.cabang_id,
            CASE WHEN sr.sr_cabang IS NULL THEN  0 ELSE  1 END AS checked, sr.sr_cabang 
            FROM pbu_all_cbg_bu AS c
            LEFT JOIN pbu_sr_cabang AS sr ON sr.cabang_id = c.cabang_id
            $on ");
            $arr = $comm->queryAll(true, $param);
            $this->renderJsonArr($arr);
        }
    }
    public function actionIndexSrCabangCmp()
    {
//        $comm = Yii::app()->db->createCommand("
//        SELECT 	sr.cabang_id, sr.kode_cabang, sr.nama_cabang
//        FROM pbu_sr_cbg_area_bu AS sr
//        WHERE bu_id = :bu_id");
//        $arr = $comm->queryAll(true, [':bu_id' => $_POST['bu_id']]);
//        $this->renderJsonArr($arr);
        $criteria = new CDbCriteria();
        $param = [];
        if (isset ($_POST['bu_id'])) {
            $criteria->addCondition('bu_id = :bu_id');
            $param[':bu_id'] = $_POST['bu_id'];
        }
//        $criteria->addCondition('checked = 1');
        $criteria->params = $param;
        $model = SrCbgAreaBu::model()->findAll($criteria);
        $total = SrCbgAreaBu::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionSinkron()
    {
        $html = "";
        $output = Yii::app()->curl->post(Yii::app()->curl->base_url . 'GetAllCabang',
            http_build_query(['bu_id' => $_POST['bu_id']]));
        $decode = json_decode($output, true);
        foreach ($decode['results'] as $result) {
            $model = Cabang::model()->findByPk($result['cabang_id']);
            if ($model == null) {
                $model = new Cabang;
            }
            $model->setAttributes($result);
            if (!$model->save()) {
                $html .= CHtml::errorSummary($model);
            }
        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => $html
        ));
        Yii::app()->end();
    }
}