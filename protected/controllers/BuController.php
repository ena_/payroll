<?php
class BuController extends GxController {
	public function actionCreate() {
		$model = new Bu;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( url( '/' ) );
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Bu'][ $k ] = $v;
			}
			$model->attributes = $_POST['Bu'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bu_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Bu' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Bu'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Bu'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bu_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->bu_id ) );
			}
		}
	}
	public function actionUpdateDataKaryawan() {
//        var_dump(Yii::app()->curl->base_url);
//        if (isset($_POST) && !empty($_POST)) {
//            $service_url = "http://localhost:81/siena/api/GetAllPtkp";
//            $curl = curl_init($service_url);
//            $curl_post_data = array(
//                "user_id" => 42,
//                "emailaddress" => 'lorna@example.com',
//            );
//            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Accept:application / json, Content - Type:application / json']);
//            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization:OHZvbjVmbi9LVkM2ZmEzZVRBU3BHQT09Ojom49gzyG55a/fi/EWvSKDL']);
//            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($curl, CURLOPT_POST, true);
//            curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
//            $curl_response = curl_exec($curl);
//            curl_close($curl);
//            echo($curl_response);
		$html   = "";
		$output = Yii::app()->curl->get( Yii::app()->curl->base_url . 'GetAllBu' );
//            var_dump(Yii::app()->curl->base_url);
		$decode = json_decode( $output, true );
		foreach ( $decode['results'] as $result ) {
			$bu = Bu::model()->findByPk( $result['bu_id'] );
			if ( $bu == null ) {
				$bu = new Bu;
			}
			if ( $result['tgl_berdiri'] == '0000-00-00' ) {
				unset( $result['tgl_berdiri'] );
			}
			$bu->setAttributes( $result );
			if ( ! $bu->save() ) {
//                Yii::log();
				$html .= CHtml::errorSummary( $bu );
			}
		}
//        echo $output;
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => $html
		) );
		Yii::app()->end();
//        }
	}
//    public function actionDelete($id)
//    {
//        if (Yii::app()->request->isPostRequest) {
//            $msg = 'Data berhasil dihapus.';
//            $status = true;
//            try {
//                $this->loadModel($id, 'Bu')->delete();
//            } catch (Exception $ex) {
//                $status = false;
//                $msg = $ex;
//            }
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg
//            ));
//            Yii::app()->end();
//        } else {
//            throw new CHttpException(400,
//                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
//        }
//    }
	public function actionPick() {
		$criteria = new CDbCriteria();
		$param    = array();
		if ( isset( $_POST['view'] ) ) {
			$criteria->addCondition( 'item_type = :item_type' );
			$param[':item_type'] = $_POST['view'];
		}
		if ( isset( $_POST['node_id'] ) ) {
			$criteria->addCondition( 'parent = :parent' );
			$param[':parent'] = $_POST['node_id'];
		}
		$criteria->params = $param;
		$model            = BuTree::model()->findAll( $criteria );
		$total            = BuTree::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$param    = array();
//        $user = Users::model()->findByPk(app()->user->getId());
//        if(!isset($_POST['query']) && !$user->isUserHolding()){
//            $cbg = Cabang::model()->findByPk(Yii::app()->user->cabang_id);
//            $criteria->addCondition('bu_id = :bu_id');
//            $param[':bu_id'] = $cbg->bu_id;
//        }
		if ( isset( $_POST['bu_id'] ) && $_POST['bu_id'] != null ) {
			$criteria->addCondition( 'bu_id = :bu_id' );
			$param[':bu_id'] = $_POST['bu_id'];
		}
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) )
		) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$criteria->params = $param;
		$model            = Bu::model()->findAll( $criteria );
		$total            = Bu::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionIndexSrBu() {
		/** @var Users $user */
		$user = Users::model()->findByPk( user()->getId() );
		$comm = Yii::app()->db->createCommand( "
        SELECT 	DISTINCT sr.bu_id,sr.bu_kode,sr.bu_nama_alias,sr.bu_name
        FROM pbu_sr_cbg_area_bu AS sr
        WHERE sr.security_roles_id = :security_roles_id" );
		$arr  = $comm->queryAll( true, [ ':security_roles_id' => $user->security_roles_id ] );
		$this->renderJsonArr( $arr );
	}
}