<!DOCTYPE html>
<html>
<head>
    <style>
        .top {
            border-style: solid;
            border-top: thin #000000;
        }

        .left {
            border-style: solid;
            border-left: thin #000000;
        }

        .right {
            border-style: solid;
            border-right: thin #000000;
        }

        .bottom {
            border-style: solid;
            border-bottom: thin #000000;
        }
    </style>
</head>
<body>
<table style="border-collapse: collapse; width: 975px;" width="755">
    <tbody>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle; width: 754px;"
            colspan="3" rowspan="3">
            <img style="display:block;" src="images/<?= $bu_id; ?>.png"/>
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: underline; text-align: left; vertical-align: middle;  "
            colspan="3">SLIP&nbsp;GAJI&nbsp;KARYAWAN
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            PERIODE
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 291px; "
            colspan="2">:&nbsp;<?= $periode; ?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            CABANG
        </td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 291px; "
            colspan="2">:&nbsp;<?= $cabang; ?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;   ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>
<table style="border-collapse: collapse; width: 975px;" width="755">
    <tbody>
    <tr>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width:5%; ">
            NIK
        </td>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 25%; ">
            :&nbsp;<?= $nik; ?></td>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 10%; ">
            KTP
        </td>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 25%; ">
            :&nbsp;<?= $ktp; ?></td>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 10%; ">
            JABATAN
        </td>
        <td class='top'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  width: 25%; ">
            :&nbsp;<?= $jabatan; ?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            NAMA
        </td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;   ">
            :&nbsp;<?= $nama; ?></td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            NPWP
        </td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;   ">
            :&nbsp;<?= $npwp; ?></td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            GRADE
        </td>
        <td style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal; ">
            :&nbsp;<?= $lvl; ?>&nbsp;<?= $gol; ?></td>
    </tr>
    <tr>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal; ">
            EMAIL
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            :&nbsp;<?= $email; ?></td>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;   ">
            STATUS
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            :&nbsp;<?= $status; ?></td>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;  ">
            HARI&nbsp;KERJA
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 10pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: justify; vertical-align: top; white-space: normal;   ">
            :&nbsp;<?= $hk; ?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;   ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;   ">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>
<table style="border-collapse: collapse; width: 975px;" width="755">
    <tbody>
    <tr>
        <td class='top'
            style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; text-align: left; vertical-align: middle;  width: 265px; "
            colspan="2">PENDAPATAN
        </td>
        <td class='top'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; ">
            &nbsp;
        </td>
        <td class='top'
            style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            POTONGAN
        </td>
        <td class='top'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
            &nbsp;
        </td>
        <td class='top'
            style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
            &nbsp;
        </td>
    </tr>
	<?php foreach ( $details as $row ) : ?>
        <tr>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; ">
                &nbsp;<?= $row['in_n']; ?></td>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; ">
                &nbsp;<?= $row['in_a']; ?></td>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; ">
                &nbsp;
            </td>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
                &nbsp;<?= $row['ot_n']; ?></td>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
                &nbsp;<?= $row['ot_a']; ?></td>
            <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
                &nbsp;
            </td>
        </tr>
	<?php endforeach; ?>
    <tr>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; ">
            Total&nbsp;Pendapatan&nbsp;
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; ">
            : &nbsp;<?= $data_inc; ?></td>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; ">
            &nbsp;
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            Total&nbsp;Potongan
        </td>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
            : &nbsp;<?= $pot; ?></td>
        <td class='bottom'
            style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 125px; ">
            Take&nbsp;Home&nbsp;Pay
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: bold; font-style: normal; text-decoration: none; vertical-align: middle;  width: 140px; ">
            : &nbsp;<?= $thp; ?></td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 89px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; "
            colspan="2">Yogyakarta,&nbsp;<?= $tgl; ?></td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: top; height: 57pt; width: 354px; "
            colspan="3" rowspan="4" height="76">Cat&nbsp;:
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; "
            colspan="2">Dibuat,
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 121px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 170px; ">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; vertical-align: middle;  width: 115px; ">
            &nbsp;
        </td>
        <td style="color: #000000; font-size: 11pt; font-weight: 400; font-style: normal; text-decoration: none; text-align: center; vertical-align: middle;  width: 291px; "
            colspan="2">Payroll
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>