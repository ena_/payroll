<h1>Laporan FingerPrint</h1>
<h3>NAMA : <?=$nama?></h3>
<h3>PERIODE : <?=$kode_periode?></h3>
<h3>CABANG : <?=$kode_cabang?></h3>
<?
$this->pageTitle = 'Laporan FingerPrint';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Unit Usaha',
            'name' => 'bu_kode'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Tanggal',
            'name' => 'selected_date'
        ),
        array(
            'header' => 'Masuk',
            'name' => 'masuk',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Keluar',
            'name' => 'keluar',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),        
        array(
            'header' => 'Ket',
            'name' => 'keterangan'
        ),
        array(
            'header' => 'Alasan',
            'name' => '-'
        )
    )
));
?>