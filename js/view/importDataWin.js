jun.ImportData = Ext.extend(Ext.Window, {
    title: "Import Komponen Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 290,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztPeriodeLib.getTotalCount() === 0) {
            jun.rztPeriodeLib.load();
        }
//        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
//            jun.rztChartMasterCmp.load();
//        }
//        if (jun.rztStoreCmp.getTotalCount() === 0) {
//            jun.rztStoreCmp.load();
//        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                xtype: 'combo',
                style: 'margin-bottom:2px',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                forceSelection: true,
                store: jun.rztPeriodeLib,
                fieldLabel: 'Periode',
                valueField: 'periode_id',
                displayField: 'kode_periode',
                itemSelector: "div.search-item",
                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                    '<span style="width:100%;display:inline-block;"><b>{kode_periode}</b>\n\
                    <br>\n\<b>Start: </b>{periode_start}\n\\n\
                    <br>\n\<b>End: </b>{periode_end}\n\</span>',
                    "</div></tpl>"),
                ref: 'periode',
                anchor: '100%'
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 100,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg'
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportData",
                border: !1
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-DownloadFormatTimeSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                hidden: true,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Download Format TimeSheet",
                    ref: "../btnDownload",
                    hidden: true
                },
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportData.superclass.initComponent.call(this);
        this.btnDownload.on("click", this.onbtnDownloadclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnDownloadclick: function () {
        // window.open(FORMAT_UPLOAD_TIMESHEET);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT TIMESHEET -----');
        jun.submitKasCounter = 0;
        this.loopPost();
    },
    loopPost: function () {
        var arrData = jun.dataStockOpname[jun.namasheet];
        var periode = this.periode.getValue();
        if (jun.submitKasCounter >= arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT TIMESHEET SELESAI -----\n'
                + 'Jumlah data : ' + (jun.submitKasCounter));
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            jun.dataStockOpname = null;
            return;
        }
        var d = arrData[jun.submitKasCounter];

        if (this.isemptyorwhitespace(d.NIK) || this.isemptyorwhitespace(d.PAYCODE) || this.isemptyorwhitespace(d.AMOUNT))
        {
            this.writeLog('DATA TIDAK LENGKAP PADA BARIS KE-'+(jun.submitKasCounter+1));
            return;
        }

        // console.log(arrData);
        // console.log(Ext.encode(arrData));
        Ext.getCmp('form-ImportData').getForm().submit({
            url: 'PayrollWorksheet/Upload2',
            timeOut: 30000,
            scope: this,
            params: {
                periode_id: periode,
                detil: Ext.encode(arrData)
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                var msg = response.msg;
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.btnSave.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    nextPost: function () {
        jun.submitKasCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('\n----- ' + (jun.submitKasCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --------------\n' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    },
    isemptyorwhitespace: function isEmptyOrSpaces(str){
        return str === null || str.match(/^ *$/) !== null;
    }
});