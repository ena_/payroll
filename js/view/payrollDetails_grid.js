jun.PayrollDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PayrollDetails",
    id: 'docs-jun.PayrollDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama Skema',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_skema'
        },
        {
            header: 'Faktor',
            sortable: true,
            resizable: true,
            dataIndex: 'type_',
            renderer: jun.renderFaktor
        },
        {
            header: 'Nilai',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPayrollDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Edit Nilai Komponen Gaji',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Komponen Gaji',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PayrollDetailsGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PayrollDetailsWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih komponen gaji");
            return;
        }
        Ext.MessageBox.prompt('Nilai komponen gaji', 'Masukkan Nilai :', function (btn, text) {
            if (text == "" || btn == 'cancel') return;
            var selectedz = this.sm.getSelected();
            var num = /^-|\d+/;
            if (num.test(text)) {
                var amount = parseFloat(text);
                selectedz.set('amount', amount);
                selectedz.commit();
                Ext.Ajax.request({
                    url: 'PayrollDetails/update/id/' + selectedz.data.payroll_detail_id,
                    method: 'POST',
                    params: {
                        id: selectedz.data.payroll_detail_id,
                        amount: selectedz.data.amount
                    },
                    success: function (f, a) {
                        jun.rztPayroll.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.getCmp('take_home_pay_id').setValue(response.take_home_pay);
                        Ext.getCmp('total_income_id').setValue(response.total_income);
                        Ext.getCmp('total_deduction_id').setValue(response.total_deduction);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            } else {
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: 'Input must number.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        }, this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PayrollDetails/delete/id/' + record.json.payroll_absensi_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPayrollAbsensi.reload();
                var response = Ext.decode(f.responseText);
                Ext.getCmp('take_home_pay_id').setValue(response.take_home_pay);
                Ext.getCmp('total_income_id').setValue(response.total_income);
                Ext.getCmp('total_deduction_id').setValue(response.total_deduction);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
