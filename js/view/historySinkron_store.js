jun.HistorySinkronstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.HistorySinkronstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'HistorySinkronStoreId',
            url: 'HistorySinkron',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'history_sinkron_id'},
                {name: 'insert_'},
                {name: 'update_'},
                {name: 'tdate'},
                {name: 'model_'},
                {name: 'user_id'},
                {name: 'status'},
                {name: 'note_'}
            ]
        }, cfg));
    }
});
jun.rztHistorySinkron = new jun.HistorySinkronstore();
//jun.rztHistorySinkron.load();
