jun.Pegawaistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pegawaistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PegawaiStoreId',
            url: 'Pegawai',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pegawai_id'},
                {name: 'nik'},
                {name: 'golongan_id'},
                {name: 'leveling_id'},
                {name: 'cabang_id'},
                {name: 'status_id'},
                {name: 'nama_lengkap'},
                {name: 'email'},
                {name: 'tgl_masuk'},
                {name: 'npwp'},
                {name: 'bank_nama'},
                {name: 'bank_kota'},
                {name: 'rekening'},
                {name: 'status_pegawai_id'},
                {name: 'jabatan_id'},
                {name: 'tdate'},
                {name: 'tuser'},
                {name: 'jenis_periode_id'},
                {name: 'last_update'},
                {name: 'last_update_id'},
                {name: 'birth_date'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztPegawai = new jun.Pegawaistore();
jun.rztPegawaiEmail = new jun.Pegawaistore({
    url: 'Pegawai/email'
});
jun.rztPegawaiCmp = new jun.Pegawaistore();
jun.rztPegawaiLib = new jun.Pegawaistore();
jun.rztPegawaiLib.load();
