jun.Lockstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Lockstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LockStoreId',
            url: 'Lock',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'lock_id'},
                {name: 'periode_id'},
                {name: 'tdate'},
                {name: 'user_id'}
            ]
        }, cfg));
    }
});
jun.rztLock = new jun.Lockstore();
jun.rztLock.load();
