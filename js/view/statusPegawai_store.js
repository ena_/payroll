jun.StatusPegawaistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StatusPegawaistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusPegawaiStoreId',
            url: 'StatusPegawai',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'status_pegawai_id'},
                {name: 'kode'},
                {name: 'nama_status'}
            ]
        }, cfg));
    }
});
jun.rztStatusPegawai = new jun.StatusPegawaistore();
jun.rztStatusPegawaiCmp = new jun.StatusPegawaistore();
jun.rztStatusPegawaiLib = new jun.StatusPegawaistore();
//jun.rztStatusPegawai.load();
