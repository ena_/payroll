jun.Fixaddstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Fixaddstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FixaddStoreId',
            url: 'Fixadd',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'add_id'},
                {name: 'kode'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztFixadd = new jun.Fixaddstore();
jun.rztFixaddCmp = new jun.Fixaddstore();
jun.rztFixaddLib = new jun.Fixaddstore();
//jun.rztFixadd.load();
