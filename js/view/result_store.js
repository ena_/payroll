jun.Resultstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Resultstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResultStoreId',
            url: 'Result',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'result_id'},
                {name: 'pin_id'},
                {name: 'periode_id'},
                {name: 'total_hari_kerja'},
                {name: 'total_lk'},
                {name: 'total_sakit'},
                {name: 'total_cuti_tahunan'},
                {name: 'total_off'},
                {name: 'total_min_lembur'},
                {name: 'locked'},
                {name: 'tdate'}
            ]
        }, cfg));
    }
});
jun.rztResult = new jun.Resultstore();
jun.rztResultLib = new jun.Resultstore();
//jun.rztResult.load();
