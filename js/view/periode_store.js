jun.PeriodeStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PeriodeStore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PeriodeStoreId',
            url: 'Periode',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'periode_id'},
                {name: 'jenis_periode_id'},
                {name: 'kode_periode'},
                {name: 'periode_start', type: 'datetime'},
                {name: 'periode_end', type: 'datetime'},
                {name: 'tdate', type: 'date'},
                {name: 'jumlah_off'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztPeriode = new jun.PeriodeStore();
jun.rztPeriodeCmp = new jun.PeriodeStore();
jun.rztPeriodeLib = new jun.PeriodeStore();
// jun.rztPeriodeLib.load();