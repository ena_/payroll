jun.Cabangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Cabangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CabangStoreId',
            url: 'Cabang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'cabang_id'},
                {name: 'kode_cabang'},
                {name: 'nama_cabang'},
                {name: 'bu_id'},
                {name: 'alamat_id'},
                {name: 'no_telp_cabang'},
                {name: 'alamat_cabang'},
                {name: 'kepala_cabang_stat'},
                {name: 'area_id'},
                {name: 'umk'},
                {name: 'npwp'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztCabang = new jun.Cabangstore();
jun.rztCabangLib = new jun.Cabangstore();
jun.rztCabangCmp = new jun.Cabangstore();
jun.rztCabangUser = new jun.Cabangstore({
    baseParams: {f: "usr"}
});
jun.CabangBustore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CabangBustore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CabangBuStoreId',
            url: 'Cabang/IndexSrCabang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'sr_cabang'},
                {name: 'cabang_id'},
                {name: 'checked', type: 'boolean'},
                {name: 'security_roles_id'},
                {name: 'kode_cabang'},
                {name: 'nama_cabang'},
                {name: 'bu_id'},
                {name: 'alamat_id'},
                {name: 'no_telp_cabang'},
                {name: 'alamat_cabang'},
                {name: 'kepala_cabang_stat'},
                {name: 'area_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'bu_name'},
                {name: 'bu_kode'}
            ]
        }, cfg));
    },
    checkAll: function () {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('checked', 1);
            record.commit(true);
        }
    },
    uncheckAll: function () {
//      var dataIndex = column.dataIndex;
        for (var i = 0; i < this.getCount(); i++) {
            var record = this.getAt(i);
            record.set('checked', 0);
            record.commit(true);
        }
    }
});
jun.rztCabangBu = new jun.CabangBustore();